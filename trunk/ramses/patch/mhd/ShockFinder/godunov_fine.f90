!###########################################################
!###########################################################
!###########################################################
!###########################################################
subroutine godunov_fine(ilevel)
  use amr_commons
  use hydro_commons
  implicit none
  integer::ilevel
  !--------------------------------------------------------------------------
  ! This routine is a wrapper to the second order Godunov solver.
  ! Small grids (2x2x2) are gathered from level ilevel and sent to the
  ! hydro solver. On entry, hydro variables are gathered from array uold.
  ! On exit, unew has been updated. 
  !--------------------------------------------------------------------------
  integer::i,ivar,igrid,ncache,ngrid
  integer,dimension(1:nvector),save::ind_grid

  if(numbtot(1,ilevel)==0)return
  if(static)return
  if(verbose)write(*,111)ilevel

  ! Loop over active grids by vector sweeps
  ncache=active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid=MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
     end do
     call godfine1(ind_grid,ngrid,ilevel)
  end do

111 format('   Entering godunov_fine for level ',i2)

end subroutine godunov_fine
!###########################################################
!###########################################################
!###########################################################
!###########################################################
subroutine set_unew(ilevel)
  use amr_commons
  use hydro_commons
  implicit none
  integer::ilevel
  !--------------------------------------------------------------------------
  ! This routine sets array unew to its initial value uold before calling
  ! the hydro scheme. unew is set to zero in virtual boundaries.
  !--------------------------------------------------------------------------
  integer::i,ivar,ind,icpu,iskip,irad
  real(dp)::d,u,v,w,e,A,B,C

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Set unew to uold for myid cells
  do ind=1,twotondim
     iskip=ncoarse+(ind-1)*ngridmax
     do ivar=1,nvar+3
        do i=1,active(ilevel)%ngrid
           unew(active(ilevel)%igrid(i)+iskip,ivar) = uold(active(ilevel)%igrid(i)+iskip,ivar)
        end do
     end do
     if(pressure_fix)then
        do i=1,active(ilevel)%ngrid
           divu(active(ilevel)%igrid(i)+iskip) = 0.0
        end do
        do i=1,active(ilevel)%ngrid
           d=max(uold(active(ilevel)%igrid(i)+iskip,1),smallr)
           u=uold(active(ilevel)%igrid(i)+iskip,2)/d
           v=uold(active(ilevel)%igrid(i)+iskip,3)/d
           w=uold(active(ilevel)%igrid(i)+iskip,4)/d
           A=0.5*(uold(active(ilevel)%igrid(i)+iskip,6)+uold(active(ilevel)%igrid(i)+iskip,nvar+1))
           B=0.5*(uold(active(ilevel)%igrid(i)+iskip,7)+uold(active(ilevel)%igrid(i)+iskip,nvar+2))
           C=0.5*(uold(active(ilevel)%igrid(i)+iskip,8)+uold(active(ilevel)%igrid(i)+iskip,nvar+3))
           e=uold(active(ilevel)%igrid(i)+iskip,5)-0.5*d*(u**2+v**2+w**2)-0.5*(A**2+B**2+C**2)
#if NENER>0
           do irad=1,nener
              e=e-uold(active(ilevel)%igrid(i)+iskip,8+irad)
           end do
#endif
           enew(active(ilevel)%igrid(i)+iskip)=e
        end do
     end if
  end do

  ! Set unew to 0 for virtual boundary cells
  do icpu=1,ncpu
  do ind=1,twotondim
     iskip=ncoarse+(ind-1)*ngridmax
     do ivar=1,nvar+3
        do i=1,reception(icpu,ilevel)%ngrid
           unew(reception(icpu,ilevel)%igrid(i)+iskip,ivar)=0.0
        end do
     end do
     if(pressure_fix)then
        do i=1,reception(icpu,ilevel)%ngrid
           divu(reception(icpu,ilevel)%igrid(i)+iskip) = 0.0
           enew(reception(icpu,ilevel)%igrid(i)+iskip) = 0.0
        end do
     end if
  end do
  end do

111 format('   Entering set_unew for level ',i2)

end subroutine set_unew
!###########################################################
!###########################################################
!###########################################################
!###########################################################
subroutine set_uold(ilevel)
  use amr_commons
  use hydro_commons
  use poisson_commons
  implicit none
  integer::ilevel
  !--------------------------------------------------------------------------
  ! This routine sets array uold to its new value unew after the
  ! hydro step.
  !--------------------------------------------------------------------------
  integer::i,ivar,ind,iskip,nx_loc,irad,ind_cell
  real(dp)::scale,d,u,v,w,A,B,C
  real(dp)::e_mag,e_kin,e_cons,e_prim,e_trunc,div,dx,fact,d_old

  integer::igrid,ncache,ngrid
  integer,dimension(1:nvector),save::ind_grid

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  nx_loc=icoarse_max-icoarse_min+1
  scale=boxlen/dble(nx_loc)
  dx=0.5d0**ilevel*scale

  ! Add gravity source terms to unew
  if(poisson)then
     call add_gravity_source_terms(ilevel)
  end if

  ! Add non conservative pdV terms to unew 
  ! for thermal and/or non-thermal energies
  if(pressure_fix.OR.nener>0)then
     call add_pdv_source_terms(ilevel)
  endif

#if NCR>0
  ! Add CR energy production at shock boundaries
  if(cr_shock)then
     ! Loop over active grids by vector sweeps
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do
        call add_pcr_shock_source_terms(ind_grid,ngrid,ilevel)
     end do
  endif
#endif

  ! Set uold to unew for myid cells
  do ind=1,twotondim
     iskip=ncoarse+(ind-1)*ngridmax
     do ivar=1,nvar+3
        do i=1,active(ilevel)%ngrid
           uold(active(ilevel)%igrid(i)+iskip,ivar) = unew(active(ilevel)%igrid(i)+iskip,ivar)
        end do
     end do
     if(pressure_fix)then
        fact=(gamma-1.0d0)
        do i=1,active(ilevel)%ngrid
           ind_cell=active(ilevel)%igrid(i)+iskip
           d=max(uold(ind_cell,1),smallr)
           u=uold(ind_cell,2)/d
           v=uold(ind_cell,3)/d
           w=uold(ind_cell,4)/d
           A=0.5*(uold(ind_cell,6)+uold(ind_cell,nvar+1))
           B=0.5*(uold(ind_cell,7)+uold(ind_cell,nvar+2))
           C=0.5*(uold(ind_cell,8)+uold(ind_cell,nvar+3))
           e_kin=0.5*d*(u**2+v**2+w**2)
#if NENER>0
           do irad=1,nener
              e_kin=e_kin+uold(ind_cell,8+irad)
           end do
#endif
           e_mag=0.5*(A**2+B**2+C**2)
           e_cons=uold(ind_cell,5)-e_kin-e_mag
           e_prim=enew(ind_cell)
           ! Note: here divu=-div.u*dt
           div=abs(divu(ind_cell))*dx/dtnew(ilevel)
           ! Estimate of the local truncation errors
           e_trunc=beta_fix*d*max(div,3.0*hexp*dx)**2
           if(e_cons<e_trunc)then
              uold(ind_cell,5)=e_prim+e_kin+e_mag
           end if
        end do
     end if
  end do

111 format('   Entering set_uold for level ',i2)

end subroutine set_uold
!###########################################################
!###########################################################
!###########################################################
!###########################################################
subroutine add_gravity_source_terms(ilevel)
  use amr_commons
  use hydro_commons
  use poisson_commons
  implicit none
  integer::ilevel
  !--------------------------------------------------------------------------
  ! This routine adds to unew the gravity source terms
  ! with only half a time step. Only the momentum and the
  ! total energy are modified in array unew.
  !--------------------------------------------------------------------------
  integer::i,ivar,ind,iskip,nx_loc,ind_cell
  real(dp)::d,u,v,w,e_kin,e_prim,d_old,fact

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Add gravity source term at time t with half time step
  do ind=1,twotondim
     iskip=ncoarse+(ind-1)*ngridmax
     do i=1,active(ilevel)%ngrid
        ind_cell=active(ilevel)%igrid(i)+iskip
        d=max(unew(ind_cell,1),smallr)
        u=0.0; v=0.0; w=0.0
        if(ndim>0)u=unew(ind_cell,2)/d
        if(ndim>1)v=unew(ind_cell,3)/d
        if(ndim>2)w=unew(ind_cell,4)/d
        e_kin=0.5*d*(u**2+v**2+w**2)
        e_prim=unew(ind_cell,5)-e_kin
        d_old=max(uold(ind_cell,1),smallr)
        fact=d_old/d*0.5*dtnew(ilevel)
        if(ndim>0)then
           u=u+f(ind_cell,1)*fact
           unew(ind_cell,2)=d*u
        endif
        if(ndim>1)then
           v=v+f(ind_cell,2)*fact
           unew(ind_cell,3)=d*v
        end if
        if(ndim>2)then
           w=w+f(ind_cell,3)*fact
           unew(ind_cell,4)=d*w
        endif
        e_kin=0.5*d*(u**2+v**2+w**2)
        unew(ind_cell,5)=e_prim+e_kin
     end do
  end do

111 format('   Entering add_gravity_source_terms for level ',i2)

end subroutine add_gravity_source_terms
!###########################################################
!###########################################################
!###########################################################
!###########################################################
subroutine add_pdv_source_terms(ilevel)
  use amr_commons
  use hydro_commons
  implicit none
  integer::ilevel
  !---------------------------------------------------------
  ! This routine adds the pdV source term to the internal
  ! energy equation and to the non-thermal energy equations.
  !---------------------------------------------------------
  integer::i,ivar,irad,ind,iskip,nx_loc,ind_cell1,igroup
  integer::ncache,igrid,ngrid,idim,id1,ig1,ih1,id2,ig2,ih2
  integer,dimension(1:3,1:2,1:8)::iii,jjj
  real(dp)::scale,dx,dx_loc,d,u,v,w,eold,A,B,C

  integer ,dimension(1:nvector),save::ind_grid,ind_cell
  integer ,dimension(1:nvector,0:twondim),save::igridn
  integer ,dimension(1:nvector,1:ndim),save::ind_left,ind_right
  real(dp),dimension(1:nvector,1:ndim,1:ndim),save::velg,veld
  real(dp),dimension(1:nvector,1:ndim),save::dx_g,dx_d
  real(dp),dimension(1:nvector),save::divu_loc
  real(dp),dimension(1:nvector,1:ndim,1:ncr),save::Ercg,Ercd,gradEcr
  real(dp),dimension(1:nvector,1:ncr),save::vstgradEcr
  real(dp)::sqrtrho
  real(dp),dimension(1:ndim),save::bc
  real(dp),dimension(1:ncr),save::bsign
  real(dp),dimension(1:nvector,1:ndim,1:ncr),save::vst
  integer::ind_cr

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel
  
  ind_cr=8
  if(twotemp)ind_cr=9

  nx_loc=icoarse_max-icoarse_min+1
  scale=boxlen/dble(nx_loc)
  dx=0.5d0**ilevel
  dx_loc=dx*scale

  velg=0.0; veld=0.0d0

  iii(1,1,1:8)=(/1,0,1,0,1,0,1,0/); jjj(1,1,1:8)=(/2,1,4,3,6,5,8,7/)
  iii(1,2,1:8)=(/0,2,0,2,0,2,0,2/); jjj(1,2,1:8)=(/2,1,4,3,6,5,8,7/)
  iii(2,1,1:8)=(/3,3,0,0,3,3,0,0/); jjj(2,1,1:8)=(/3,4,1,2,7,8,5,6/)
  iii(2,2,1:8)=(/0,0,4,4,0,0,4,4/); jjj(2,2,1:8)=(/3,4,1,2,7,8,5,6/)
  iii(3,1,1:8)=(/5,5,5,5,0,0,0,0/); jjj(3,1,1:8)=(/5,6,7,8,1,2,3,4/)
  iii(3,2,1:8)=(/0,0,0,0,6,6,6,6/); jjj(3,2,1:8)=(/5,6,7,8,1,2,3,4/)

  ! Loop over myid grids by vector sweeps
  ncache=active(ilevel)%ngrid
  do igrid=1,ncache,nvector

     ! Gather nvector grids
     ngrid=MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
     end do

     ! Gather neighboring grids
     do i=1,ngrid
        igridn(i,0)=ind_grid(i)
     end do
     do idim=1,ndim
        do i=1,ngrid
           ind_left (i,idim)=nbor(ind_grid(i),2*idim-1)
           ind_right(i,idim)=nbor(ind_grid(i),2*idim  )
           igridn(i,2*idim-1)=son(ind_left (i,idim))
           igridn(i,2*idim  )=son(ind_right(i,idim))
        end do
     end do

     ! Loop over cells
     do ind=1,twotondim

        ! Compute central cell index
        iskip=ncoarse+(ind-1)*ngridmax
        do i=1,ngrid
           ind_cell(i)=iskip+ind_grid(i)
        end do

        ! Gather all neighboring velocities
        do idim=1,ndim
           id1=jjj(idim,1,ind); ig1=iii(idim,1,ind)
           ih1=ncoarse+(id1-1)*ngridmax
           do i=1,ngrid
!!$              if(ind_grid(i)==43)write(*,'(A,3I10)')'add_pdv left:',ind_grid(i),ind_cell(i),igridn(i,ig1)+ih1
              if(igridn(i,ig1)>0)then
                 velg(i,idim,1:ndim) = uold(igridn(i,ig1)+ih1,2:ndim+1)/max(uold(igridn(i,ig1)+ih1,1),smallr)
                 dx_g(i,idim) = dx_loc
#if NCR>0
                 if(streaming_heating)then
                    do igroup=1,ncr
                       Ercg(i,idim,igroup) = uold(igridn(i,ig1)+ih1,ind_cr+igroup)
                    end do
                 endif
#endif
              else
                 velg(i,idim,1:ndim) = uold(ind_left(i,idim),2:ndim+1)/max(uold(ind_left(i,idim),1),smallr)
                 dx_g(i,idim) = dx_loc*1.5_dp
#if NCR>0
                 if(streaming_heating)then
                    do igroup=1,ncr
                       Ercg(i,idim,igroup) = uold(ind_left(i,idim),ind_cr+igroup)
                    end do
                 endif
#endif
              end if
           enddo
           id2=jjj(idim,2,ind); ig2=iii(idim,2,ind)
           ih2=ncoarse+(id2-1)*ngridmax
           do i=1,ngrid
!!$              if(ind_grid(i)==43)write(*,'(A,3I10)')'add_pdv right:',ind_grid(i),ind_cell(i),igridn(i,ig2)+ih2
              if(igridn(i,ig2)>0)then
                 veld(i,idim,1:ndim)= uold(igridn(i,ig2)+ih2,2:ndim+1)/max(uold(igridn(i,ig2)+ih2,1),smallr)
                 dx_d(i,idim)=dx_loc
#if NCR>0
                 if(streaming_heating)then
                    do igroup=1,ncr
                       Ercd(i,idim,igroup) = uold(igridn(i,ig2)+ih2,ind_cr+igroup)
                    end do
                 endif
#endif
              else 
                 veld(i,idim,1:ndim)= uold(ind_right(i,idim),2:ndim+1)/max(uold(ind_right(i,idim),1),smallr)
                 dx_d(i,idim)=dx_loc*1.5_dp
#if NCR>0
                 if(streaming_heating)then
                    do igroup=1,ncr
                       Ercd(i,idim,igroup) = uold(ind_right(i,idim),ind_cr+igroup)
                    end do
                 endif
#endif
              end if
           enddo
        end do
        ! End loop over dimensions

        ! Compute divu = Trace G
        divu_loc(1:ngrid)=0.0d0
        do i=1,ngrid
           do idim=1,ndim
              divu_loc(i) = divu_loc(i) + (veld(i,idim,idim)-velg(i,idim,idim)) &
                   &                    / (dx_g(i,idim)     +dx_d(i,idim))
#if NCR>0
              do igroup=1,ncr
                 gradEcr(i,idim,igroup) = (Ercd(i,idim,igroup)-Ercg(i,idim,igroup))/(dx_g(i,idim)+dx_d(i,idim))
              end do
#endif
           enddo
        end do

        ! Update thermal internal energy 
        if(pressure_fix)then
           do i=1,ngrid
              ! Compute old thermal energy
              d=max(uold(ind_cell(i),1),smallr)
              u=0.0; v=0.0; w=0.0
              if(ndim>0)u=uold(ind_cell(i),2)/d
              if(ndim>1)v=uold(ind_cell(i),3)/d
              if(ndim>2)w=uold(ind_cell(i),4)/d
              A=0.5*(uold(ind_cell(i),6)+uold(ind_cell(i),nvar+1))
              B=0.5*(uold(ind_cell(i),7)+uold(ind_cell(i),nvar+2))
              C=0.5*(uold(ind_cell(i),8)+uold(ind_cell(i),nvar+3))
              eold=uold(ind_cell(i),5)-0.5*d*(u**2+v**2+w**2)-0.5*(A**2+B**2+C**2)
#if NENER>0
              do irad=1,nener
                 eold=eold-uold(ind_cell(i),8+irad)
              end do
#endif
              ! Add -pdV term
              enew(ind_cell(i))=enew(ind_cell(i)) &
                   & -(gamma-1.0d0)*eold*divu_loc(i)*dtnew(ilevel)
           end do
        end if
#if NCR>0
        if(streaming_heating)then
           vstgradEcr=0.0d0
           do i=1,ngrid
              sqrtrho=sqrt(uold(ind_cell(i),1))
              do idim=1,ndim
                 bc(idim)=0.5d0*(uold(ind_cell(i),5+idim)+uold(ind_cell(i),nvar+idim))
              enddo
              bsign=0.0d0
              do igroup=1,ncr
                 do idim=1,ndim
                    bsign(igroup)=bsign(igroup)+bc(idim)*gradEcr(i,idim,igroup)
                 enddo
              enddo
              do igroup=1,ncr
                 if(bsign(igroup).eq.0.0d0)then
                    vst(i,1:ndim,igroup)=0.0d0
                 else
                    bsign(igroup)=bsign(igroup)/abs(bsign(igroup))
                    do idim=1,ndim
                       vst(i,idim,igroup)=-bsign(igroup)*bc(idim)/sqrtrho*fudge_streamboost
                    enddo
                 endif
              enddo
              do idim=1,ndim
                 do igroup=1,ncr
                    vstgradEcr(i,igroup) = vstgradEcr(i,igroup) + vst(i,idim,igroup)*gradEcr(i,idim,igroup)
!!$                    write(*,'(A,i10,3e11.3)')'grad stuff',ind_cell(i),vst(i,idim,igroup),gradEcr(i,idim,igroup),vstgradEcr(i,igroup)
                 enddo
              end do
           end do
        endif
#endif

#if NENER>0
        do irad=1,nener
           do i=1,ngrid
              ! Add -pdV term
              unew(ind_cell(i),8+irad)=unew(ind_cell(i),8+irad) &
                   & -(gamma_rad(irad)-1.0d0)*uold(ind_cell(i),8+irad)*divu_loc(i)*dtnew(ilevel)
           end do
        end do
#endif

#if NENER>NGRP && NCR>0
        if(streaming_heating)then
           do i=1,ngrid
              ! Add CR streaming heating term
              do igroup=1,ncr
                 unew(ind_cell(i),ind_cr+igroup)=unew(ind_cell(i),ind_cr+igroup) &
                      &  +(gamma_rad(igroup)-1.0d0)*vstgradEcr(i,igroup)*dtnew(ilevel)
!!$                 unew(ind_cell(i),ind_cr+igroup)=MAX(unew(ind_cell(i),ind_cr+igroup),TCRmin)
                 unew(ind_cell(i),ind_cr+igroup)=MAX(unew(ind_cell(i),ind_cr+igroup),0.0d0)
              end do
           end do
        endif
#endif

     enddo
     ! End loop over cells
  end do
  ! End loop over grids


111 format('   Entering add_pdv_source_terms for level ',i2)

end subroutine add_pdv_source_terms
!###########################################################
!###########################################################
!###########################################################
!###########################################################
subroutine add_pcr_shock_source_terms(ind_grid,ncache,ilevel)
  use amr_commons
  use hydro_commons
  implicit none
  integer::ilevel,ncache
  integer,dimension(1:nvector)::ind_grid
  !-------------------------------------------------------------------
  !-------------------------------------------------------------------
  integer ,dimension(1:nvector,1:threetondim     ),save::nbors_father_cells
  integer ,dimension(1:nvector,1:twotondim       ),save::nbors_father_grids
  integer ,dimension(1:nvector,0:twondim         ),save::ibuffer_father
  integer ,dimension(1:nvector,0:twondim         ),save::ind1
  real(dp),dimension(1:nvector,0:twondim  ,1:nvar+3),save::u1
  real(dp),dimension(1:nvector,1:twotondim,1:nvar+3),save::u2
  real(dp),dimension(1:nvector,1:twotondim,1:nvar+3),save::u3 !store interpolated divu and enew

  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar+3),save::uloc
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2),save::divuloc,enewloc,moldloc,ddx
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2),save::pres,csou,dens,geff,ethe,uvel
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:ndim),save::gradd,gradT,gradS
  real(dp),dimension(1:ndim)::unorm,urescaled
  real(dp),dimension(1:twotondim),save::vol
  real(dp),dimension(1:ndim),save::x,dd,dg,position
  real(dp),dimension(iu1:iu2,ju1:ju2,ku1:ku2),save::quantity
  real(dp),dimension(1:nener),save::ecr_pre,ecr_pos
  real(dp),dimension(1:ndim)::x3

  logical ,dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2),save::okshock

  integer,dimension(1:nvector),save::igrid_nbor,ind_cell,ind_buffer,ind_exist,ind_nexist
  integer,dimension(1:ndim),save::itarget
  integer,dimension(1:twotondim),save::icic,jcic,kcic
  integer,dimension(1:ndim),save::iig,iid

  integer::neul=5
  integer::ind_buffer1,ind_buffer2,ind_buffer3
  integer::ind_father1,ind_father2,ind_father3
  integer::i,j,ivar,idim,ind_son,ind_father,iskip,nbuffer,ibuffer
  integer::i0,j0,k0,i1,j1,k1,i2,j2,k2,i3,j3,k3,nx_loc,nb_noneigh,nexist
  integer::i1min,i1max,j1min,j1max,k1min,k1max
  integer::i2min,i2max,j2min,j2max,k2min,k2max
  integer::i3min,i3max,j3min,j3max,k3min,k3max
  integer::irad,ii,jj,kk,ig,id,jg,jd,kg,kd,icell
  integer::ipre,ipos,jpre,jpos,idim_max,ind_cr,ind_mach

  real(dp)::dx,dx_loc,scale,oneoverd,oneoverdeltax
  real(dp)::d,u,v,w,A,B,C,e,mach,mach2,d_up,csound,cr_flux,deltae,machold,machloc,mymax
  real(dp)::Ptot,pratio,Rcomp,ediss,eth_pos,eth_pre
  real(dp)::max_loc,factor,divu_pre,divu_pos,u_up
  real(dp)::dens_pre,dens_pos,dens_pre_far,dens_pos_far,dens_pre_farfar,dens_pos_farfar
  real(dp)::pres_pre,pres_pos,pres_pre_far,pres_pos_far,pres_pre_farfar,pres_pos_farfar
  real(dp)::csou_pre,csou_pos,csou_pre_far,csou_pos_far,csou_pre_farfar,csou_pos_farfar
  real(dp)::geff_pre,geff_pos,geff_pre_far,geff_pos_far,geff_pre_farfar,geff_pos_farfar
  real(dp)::uvel_pre,uvel_pos,uvel_pre_far,uvel_pos_far,uvel_pre_farfar,uvel_pos_farfar
  real(dp)::densg,densd,tempg,tempd,entrg,entrd,dTdd,dTdS,normgradT,gamma_eff
  real(dp)::minmach=1.5,nearlytwo=1.999d0,nearlythree=2.999d0
  real(dp)::cic_interpol
  real(dp)::x_pos,x_pre,y_pos,y_pre,z_pos,z_pre

  logical::okdivu

  ind_mach=9+nener
  ind_cr=8
  if(twotemp)ind_cr=9

  okshock=.true.

  ! Mesh spacing in that level
  nx_loc=icoarse_max-icoarse_min+1
  scale=boxlen/dble(nx_loc)
  dx=0.5D0**ilevel
  dx_loc=dx*scale

  ! Integer constants
  i1min=0; i1max=0; i2min=0; i2max=0; i3min=1; i3max=1
  j1min=0; j1max=0; j2min=0; j2max=0; j3min=1; j3max=1
  k1min=0; k1max=0; k2min=0; k2max=0; k3min=1; k3max=1
  if(ndim>0)then
     i1max=2; i2max=1; i3max=2
  end if
  if(ndim>1)then
     j1max=2; j2max=1; j3max=2
  end if
  if(ndim>2)then
     k1max=2; k2max=1; k3max=2
  end if

  !------------------------------------------
  ! Gather 3^ndim neighboring father cells
  !------------------------------------------
  do i=1,ncache
     ind_cell(i)=father(ind_grid(i))
  end do
  call get3cubefather(ind_cell,nbors_father_cells,nbors_father_grids,ncache,ilevel)
  
  !---------------------------
  ! Gather 6x6x6 cells stencil
  !---------------------------
  ! Loop over 3x3x3 neighboring father cells
  do k1=k1min,k1max
  do j1=j1min,j1max
  do i1=i1min,i1max
     
     ! Check if neighboring grid exists
     nbuffer=0
     nexist=0
     ind_father=1+i1+3*j1+9*k1
     do i=1,ncache
        igrid_nbor(i)=son(nbors_father_cells(i,ind_father))
        if(igrid_nbor(i)>0) then
           nexist=nexist+1
           ind_exist(nexist)=i
        else
          nbuffer=nbuffer+1
          ind_nexist(nbuffer)=i
          ind_buffer(nbuffer)=nbors_father_cells(i,ind_father)
        end if
     end do
     
     ! If not, interpolate variables from parent cells
     if(nbuffer>0)then
        call getnborfather(ind_buffer,ibuffer_father,nbuffer,ilevel)
        do j=0,twondim
           do ivar=1,nvar+3
              do i=1,nbuffer
                 u1(i,j,ivar)=uold(ibuffer_father(i,j),ivar)
              end do
           end do
           do i=1,nbuffer
              ind1(i,j)=son(ibuffer_father(i,j))
           end do
        end do
        call interpol_hydro(u1,ind1,u2,nbuffer)
        u1=0.0d0
        do j=0,twondim
           do i=1,nbuffer
              u1(i,j,1)=divu(ibuffer_father(i,j))
              ! Compute the new temperature and store
              d=max(unew(ibuffer_father(i,j),1),smallr)
              oneoverd=1d0/d
              u=unew(ibuffer_father(i,j),2)*oneoverd
              v=unew(ibuffer_father(i,j),3)*oneoverd
              w=unew(ibuffer_father(i,j),4)*oneoverd
              A=0.5*(unew(ibuffer_father(i,j),6)+unew(ibuffer_father(i,j),nvar+1))
              B=0.5*(unew(ibuffer_father(i,j),7)+unew(ibuffer_father(i,j),nvar+2))
              C=0.5*(unew(ibuffer_father(i,j),8)+unew(ibuffer_father(i,j),nvar+3))
              e=unew(ibuffer_father(i,j),5)-0.5*d*(u**2+v**2+w**2)-0.5*(A**2+B**2+C**2)
#if NENER>0
              do irad=1,nener
                 e=e-unew(ibuffer_father(i,j),8+irad) ! Here we keep only the ion temperature (should we keep the mean temperature? with CR?)
              end do
#endif
              u1(i,j,2)=e
              u1(i,j,3)=uold(ibuffer_father(i,j),ind_mach)/MAX(uold(ibuffer_father(i,j),1),smallr)
           end do
        end do
        call interpol_hydro(u1,ind1,u3,nbuffer)
     endif

     ! Loop over 2x2x2 cells
     do k2=k2min,k2max
     do j2=j2min,j2max
     do i2=i2min,i2max

        ind_son=1+i2+2*j2+4*k2
        iskip=ncoarse+(ind_son-1)*ngridmax
        do i=1,nexist
           ind_cell(i)=iskip+igrid_nbor(ind_exist(i))
        end do
        
        i3=1; j3=1; k3=1
        if(ndim>0)i3=1+2*(i1-1)+i2
        if(ndim>1)j3=1+2*(j1-1)+j2
        if(ndim>2)k3=1+2*(k1-1)+k2
        
        ! Gather hydro variables
        do ivar=1,nvar+3
           do i=1,nexist
              uloc(ind_exist(i),i3,j3,k3,ivar)=uold(ind_cell(i),ivar)
           end do
           do i=1,nbuffer
              uloc(ind_nexist(i),i3,j3,k3,ivar)=u2(i,ind_son,ivar)
           end do
        end do

        ! Gather divu
        do i=1,nexist
           divuloc(ind_exist(i),i3,j3,k3)=divu(ind_cell(i))
           moldloc(ind_exist(i),i3,j3,k3)=uold(ind_cell(i),ind_mach)/MAX(uold(ind_cell(i),1),smallr)
           ddx(ind_exist(i),i3,j3,k3)=dx_loc
        end do
        do i=1,nbuffer
           divuloc(ind_nexist(i),i3,j3,k3)=u3(i,ind_son,1)
           moldloc(ind_nexist(i),i3,j3,k3)=u3(i,ind_son,3)
           if(interpol_type.gt.0)then
              ddx(ind_nexist(i),i3,j3,k3)=dx_loc
           else
              ddx(ind_nexist(i),i3,j3,k3)=1.5d0*dx_loc
           endif
        end do
        
        ! Gather new temperature
        do i=1,nexist
           d=max(unew(ind_cell(i),1),smallr)
           oneoverd=1d0/d
           u=unew(ind_cell(i),2)*oneoverd
           v=unew(ind_cell(i),3)*oneoverd
           w=unew(ind_cell(i),4)*oneoverd
           A=0.5*(unew(ind_cell(i),6)+unew(ind_cell(i),nvar+1))
           B=0.5*(unew(ind_cell(i),7)+unew(ind_cell(i),nvar+2))
           C=0.5*(unew(ind_cell(i),8)+unew(ind_cell(i),nvar+3))
           e=unew(ind_cell(i),5)-0.5*d*(u**2+v**2+w**2)-0.5*(A**2+B**2+C**2)
#if NENER>0
           do irad=1,nener
              e=e-unew(ind_cell(i),8+irad) ! Here we keep only the ion temperature (should we keep the mean temperature? with CR?)
           end do
#endif
           enewloc(ind_exist(i),i3,j3,k3)=e
        end do
        do i=1,nbuffer
           enewloc(ind_nexist(i),i3,j3,k3)=u3(i,ind_son,2)
        end do

     end do
     end do
     end do
     ! End loop over cells

  end do
  end do
  end do
  ! End loop over neighboring grids


  !Warning: replace ijk2minmax=[0,1] by [1,2]
  ! Integer constants
  i2min=1; i2max=1
  j2min=1; j2max=1
  k2min=1; k2max=1
  if(ndim>0)then
     i1min=1;i2max=2
  end if
  if(ndim>1)then
     j2min=1;j2max=2
  end if
  if(ndim>2)then
     k2min=1;k2max=2
  end if

  ! =====================================================================
  ! Compute gradT and gradS
  ! =====================================================================
#if NVAR>8+NENER
  ! Set the mach number (and other analysis quantities) to zero everywhere
  do ivar=9+nener,nvar
  do k2=k2min,k2max
  do j2=j2min,j2max
  do i2=i2min,i2max
     do i=1,ncache
        if(ndim==1)ind_son=1+(i2-1)
        if(ndim==2)ind_son=1+(i2-1)+2*(j2-1)
        if(ndim==3)ind_son=1+(i2-1)+2*(j2-1)+4*(k2-1)
        iskip=ncoarse+(ind_son-1)*ngridmax
        ind_cell(i)=iskip+ind_grid(i)
        unew(ind_cell(i),ivar)=0.0d0
     enddo
  enddo
  enddo
  enddo
  enddo
#endif

  ! =====================================================================
  ! Compute gradT and gradS
  ! =====================================================================
  do idim=1,ndim
  if(idim==1)then
     ii=1;jj=0;kk=0 ! jg=jd, kg=kd
  endif
  if(idim==2)then
     ii=0;jj=1;kk=0 ! ig=id, kg=kd
  endif
  if(idim==3)then
     ii=0;jj=0;kk=1 ! ig=id, jg=jd
  endif
  do k2=k2min,k2max
  kg=k2-kk;kd=k2+kk
  do j2=j2min,j2max
  jg=j2-jj;jd=j2+jj
  do i2=i2min,i2max
  ig=i2-ii;id=i2+ii
     do i=1,ncache
        ! Right; Top; Front
        d=max(uloc(i,id,jd,kd,1),smallr)
        oneoverd=1d0/d
        u=uloc(i,id,jd,kd,2)*oneoverd
        v=uloc(i,id,jd,kd,3)*oneoverd
        w=uloc(i,id,jd,kd,4)*oneoverd
        A=0.5*(uloc(i,id,jd,kd,6)+uloc(i,id,jd,kd,nvar+1))
        B=0.5*(uloc(i,id,jd,kd,7)+uloc(i,id,jd,kd,nvar+2))
        C=0.5*(uloc(i,id,jd,kd,8)+uloc(i,id,jd,kd,nvar+3))
        e=uloc(i,id,jd,kd,5)-0.5*d*(u**2+v**2+w**2)-0.5*(A**2+B**2+C**2)
#if NENER>0
        do irad=1,nener
           e=e-uloc(i,id,jd,kd,8+irad)
        end do
#endif
        densd=d
        tempd=e*(gamma-1d0)*oneoverd
        entrd=e*oneoverd**(gamma-1d0)
#if NENER>0
        do irad=1,nener
           tempd=tempd+uloc(i,id,jd,kd,8+irad)*(gamma_rad(irad)-1d0)*oneoverd
           entrd=entrd+uloc(i,id,jd,kd,8+irad)*oneoverd**(gamma_rad(irad)-1d0)
        end do
#endif

        ! Left; Bottom; Back
        d=max(uloc(i,ig,jg,kg,1),smallr)
        oneoverd=1d0/d
        u=uloc(i,ig,jg,kg,2)*oneoverd
        v=uloc(i,ig,jg,kg,3)*oneoverd
        w=uloc(i,ig,jg,kg,4)*oneoverd
        A=0.5*(uloc(i,ig,jg,kg,6)+uloc(i,ig,jg,kg,nvar+1))
        B=0.5*(uloc(i,ig,jg,kg,7)+uloc(i,ig,jg,kg,nvar+2))
        C=0.5*(uloc(i,ig,jg,kg,8)+uloc(i,ig,jg,kg,nvar+3))
        e=uloc(i,ig,jg,kg,5)-0.5*d*(u**2+v**2+w**2)-0.5*(A**2+B**2+C**2)
#if NENER>0
        do irad=1,nener
           e=e-uloc(i,ig,jg,kg,8+irad) ! Here we keep only the ion temperature (should we keep the mean temperature? with CR?)
        end do
#endif
        densg=d
        tempg=e*(gamma-1d0)*oneoverd
        entrg=e*oneoverd**(gamma-1d0)
#if NENER>0
        do irad=1,nener
           tempg=tempg+uloc(i,ig,jg,kg,8+irad)*(gamma_rad(irad)-1d0)*oneoverd
           entrg=entrg+uloc(i,ig,jg,kg,8+irad)*oneoverd**(gamma_rad(irad)-1d0)
        end do
#endif

        oneoverdeltax=1d0/(ddx(i,id,jd,kd)+ddx(i,ig,jg,kg))

        gradd(i,i2,j2,k2,idim)=(densd-densg)*oneoverdeltax ! Grad range from i=[i2min,i2max]=[1,2], j=[j2min,j2max], k=[k2min,k2max]
        gradT(i,i2,j2,k2,idim)=(tempd-tempg)*oneoverdeltax
        gradS(i,i2,j2,k2,idim)=(entrd-entrg)*oneoverdeltax
     enddo
  enddo
  enddo
  enddo
  enddo   

  ! =====================================================================
  ! Tag cells with gradT.gradS>0
  ! =====================================================================
  do k2=k2min,k2max
  do j2=j2min,j2max
  do i2=i2min,i2max
     do i=1,ncache
        dTdS=0.0d0
        do idim=1,ndim
           dTdS=dTdS+gradT(i,i2,j2,k2,idim)*gradS(i,i2,j2,k2,idim)
        enddo
        dTdd=0.0d0
        do idim=1,ndim
           dTdd=dTdd+gradT(i,i2,j2,k2,idim)*gradd(i,i2,j2,k2,idim)
        enddo
        okshock(i,i2,j2,k2)=okshock(i,i2,j2,k2).and.dTdS>0.0d0.and.dTdd>0.0d0
     enddo
  enddo
  enddo
  enddo   

  ! =====================================================================
  ! Find the normal to the shock
  ! if it doesn't exist (gradT=0) prevent cell to be candidate for shock
  ! =====================================================================
  do k2=k2min,k2max
  do j2=j2min,j2max
  do i2=i2min,i2max
     do i=1,ncache
        normgradT=0.0d0
        do idim=1,ndim
           normgradT=normgradT+gradT(i,i2,j2,k2,idim)**2
        enddo
        if(normgradT>0.0d0)then
           normgradT=sqrt(normgradT)
           do idim=1,ndim
              gradT(i,i2,j2,k2,idim)=gradT(i,i2,j2,k2,idim)/normgradT
           enddo
        else
           okshock(i,i2,j2,k2)=.false.
        endif
     enddo
  enddo
  enddo
  enddo   

  ! =====================================================================
  ! Tag cells with local minimum of div.u along the normal to the schock
  ! =====================================================================
  do k2=k2min,k2max
  do j2=j2min,j2max
  do i2=i2min,i2max
     do i=1,ncache
        divu_pre=0.0d0;divu_pos=0.0d0

        normgradT=0.0d0
        do idim=1,ndim
           normgradT=normgradT+gradT(i,i2,j2,k2,idim)**2
        enddo
        if(normgradT>0.0d0)then

           position(1:ndim)=-gradT(i,i2,j2,k2,1:ndim)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=divuloc(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           divu_pre=cic_interpol(position,quantity,i2,j2,k2)

           position(1:ndim)= gradT(i,i2,j2,k2,1:ndim)
           divu_pos=cic_interpol(position,quantity,i2,j2,k2)

           okdivu=(divuloc(i,i2,j2,k2)>divu_pre) .and. (divuloc(i,i2,j2,k2)>divu_pos)
           okshock(i,i2,j2,k2)=okshock(i,i2,j2,k2).and.okdivu
           
#if NVAR>8+NENER
!!$           i3=1+i2
!!$           j3=1+j2
!!$           k3=1+k2
!!$           if(ndim==1)ind_son=1+(i2-1)
!!$           if(ndim==2)ind_son=1+(i2-1)+2*(j2-1)
!!$           if(ndim==3)ind_son=1+(i2-1)+2*(j2-1)+4*(k2-1)
!!$           iskip=ncoarse+(ind_son-1)*ngridmax
!!$           ind_cell(i)=iskip+ind_grid(i)
!!$           if(okdivu)then
!!$              unew(ind_cell(i),11+nener+ndim+4)=max(unew(ind_cell(i),1),smallr)
!!$           else
!!$              unew(ind_cell(i),11+nener+ndim+4)=0.0d0
!!$           endif
!!$           unew(ind_cell(i),11+nener+ndim+5)=divu_pre*max(unew(ind_cell(i),1),smallr)
!!$           unew(ind_cell(i),11+nener+ndim+6)=divu_pos*max(unew(ind_cell(i),1),smallr)
#endif

        endif


     enddo
  enddo
  enddo
  enddo   

  ! =====================================================================
  ! Store some useful quantities
  ! pres: total pressure (thermal + non-thermal)
  ! ethe: thermal energy
  ! dens: density
  ! csou: effective sound speed (thermal + non-thermal)
  ! geff: effective adiabatic index
  ! =====================================================================
!!$  do k2=k2min,k2max
!!$  do j2=j2min,j2max
!!$  do i2=i2min,i2max
  do k2=ku1,ku2
  do j2=ju1,ju2
  do i2=iu1,iu2
     do i=1,ncache
        d=max(uloc(i,i2,j2,k2,1),smallr)
        oneoverd=1d0/d
        u=uloc(i,i2,j2,k2,2)*oneoverd
        v=uloc(i,i2,j2,k2,3)*oneoverd
        w=uloc(i,i2,j2,k2,4)*oneoverd
        A=0.5*(uloc(i,i2,j2,k2,6)+uloc(i,i2,j2,k2,nvar+1))
        B=0.5*(uloc(i,i2,j2,k2,7)+uloc(i,i2,j2,k2,nvar+2))
        C=0.5*(uloc(i,i2,j2,k2,8)+uloc(i,i2,j2,k2,nvar+3))
        e=uloc(i,i2,j2,k2,5)-0.5*d*(u**2+v**2+w**2)-0.5*(A**2+B**2+C**2)
#if NENER>0
        do irad=1,nener
           e=e-uloc(i,i2,j2,k2,8+irad)
        end do
#endif
!!$        ethe(i,i2,j2,k2)=e+0.5d0*d*(u*u+v*v+w*w)
        ethe(i,i2,j2,k2)=e
        dens(i,i2,j2,k2)=d
        pres(i,i2,j2,k2)=e*(gamma-1d0)
        csou(i,i2,j2,k2)=gamma*(gamma-1d0)*e*oneoverd ! cs^2
        geff(i,i2,j2,k2)=gamma* e*(gamma-1d0)
        uvel(i,i2,j2,k2)=DSQRT(u**2+v**2+w**2)
        Ptot=e*(gamma-1d0)
#if NENER>0
        do irad=1,nener
           pres(i,i2,j2,k2)=pres(i,i2,j2,k2)+uloc(i,i2,j2,k2,8+irad)*(gamma_rad(irad)-1d0)
           csou(i,i2,j2,k2)=csou(i,i2,j2,k2)+gamma_rad(irad)*(gamma_rad(irad)-1d0)*uloc(i,i2,j2,k2,8+irad)*oneoverd
           geff(i,i2,j2,k2)=geff(i,i2,j2,k2)+gamma_rad(irad) *uloc(i,i2,j2,k2,8+irad)*(gamma_rad(irad)-1d0)
           Ptot=Ptot+uloc(i,i2,j2,k2,8+irad)*(gamma_rad(irad)-1d0)
        end do
#endif
        csou(i,i2,j2,k2)=sqrt(csou(i,i2,j2,k2))
        geff(i,i2,j2,k2)=geff(i,i2,j2,k2)/Ptot
     enddo
  enddo
  enddo
  enddo   

  ! =====================================================================
  ! Compute the Mach number from pressure jump
  ! =====================================================================
  do k2=k2min,k2max
  do j2=j2min,j2max
  do i2=i2min,i2max
     do i=1,ncache
        if(okshock(i,i2,j2,k2))then
        divu_pre=0.0d0;divu_pos=0.0d0

        normgradT=0.0d0
        do idim=1,ndim
           normgradT=normgradT+gradT(i,i2,j2,k2,idim)**2
        enddo
        if(normgradT>0.0d0)then

           position(1:ndim)=-gradT(i,i2,j2,k2,1:ndim)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=pres(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           pres_pre=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=csou(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           csou_pre=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=dens(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           dens_pre=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=geff(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           geff_pre=cic_interpol(position,quantity,i2,j2,k2)
           ! Warning: CIC should be done on the 3-components of the field...
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=uvel(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           uvel_pre=cic_interpol(position,quantity,i2,j2,k2)

           position(1:ndim)= gradT(i,i2,j2,k2,1:ndim)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=pres(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           pres_pos=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=csou(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           csou_pos=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=dens(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           dens_pos=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=geff(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           geff_pos=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=uvel(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           uvel_pos=cic_interpol(position,quantity,i2,j2,k2)


           position(1:ndim)=-gradT(i,i2,j2,k2,1:ndim)*nearlytwo
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=pres(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           pres_pre_far=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=csou(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           csou_pre_far=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=dens(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           dens_pre_far=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=geff(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           geff_pre_far=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=uvel(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           uvel_pre_far=cic_interpol(position,quantity,i2,j2,k2)

           position(1:ndim)= gradT(i,i2,j2,k2,1:ndim)*nearlytwo
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=pres(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           pres_pos_far=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=csou(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           csou_pos_far=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=dens(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           dens_pos_far=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=geff(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           geff_pos_far=cic_interpol(position,quantity,i2,j2,k2)
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=uvel(i,iu1:iu2,ju1:ju2,ku1:ku2)           
           uvel_pos_far=cic_interpol(position,quantity,i2,j2,k2)

           if (pres_pre .lt. pres_pre_far)then
              gamma_eff=geff_pre
           else
              gamma_eff=geff_pre_far
           endif
           pres_pre=MIN(pres_pre,pres_pre_far)
           pres_pos=MAX(pres_pos,pres_pos_far)
           csou_pre=MIN(csou_pre,csou_pre_far)
           csou_pos=MAX(csou_pos,csou_pos_far)
           dens_pre=MIN(dens_pre,dens_pre_far)
           dens_pos=MAX(dens_pos,dens_pos_far)
           if(ABS(uvel_pos_far)>ABS(uvel_pos))uvel_pos=uvel_pos_far
           if(ABS(uvel_pre_far)<ABS(uvel_pre))uvel_pre=uvel_pre_far
!!$           MIN(ABS(uvel(i,i2,j2,k2),ABS(uvel_pos),ABS(uvel_pos_far))*SIGN() ! Take into account the central cell velocity
!!$           uvel_pos=ABS(uvel_pos)
!!$           uvel_pre=ABS(uvel_pre)


!!$           ! ======= Try 3 cells ===================
!!$           ! Find the principal cartesian component of gradT
!!$           x3(1:ndim)=gradT(i,i2,j2,k2,1:ndim)*nearlythree
!!$#if NDIM==1
!!$           x_pos=dble(i2)+x3(1)
!!$           x_pre=dble(i2)-x3(1)
!!$           if(x_pos.le.4d0 .and. x_pos.ge.-1d0)then
!!$              position(1:ndim)= gradT(i,i2,j2,k2,1:ndim)*nearlythree
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=pres(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              pres_pos_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=csou(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              csou_pos_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=dens(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              dens_pos_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=geff(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              geff_pos_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$           else
!!$              pres_pos_farfar=pres_pos
!!$              csou_pos_farfar=csou_pos
!!$              dens_pos_farfar=dens_pos
!!$              geff_pos_farfar=geff_pos
!!$           endif
!!$           if(x_pre.le.4d0 .and. x_pre.ge.-1d0)then
!!$              position(1:ndim)= gradT(i,i2,j2,k2,1:ndim)*nearlythree
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=pres(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              pres_pre_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=csou(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              csou_pre_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=dens(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              dens_pre_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=geff(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              geff_pre_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$           else
!!$              pres_pre_farfar=pres_pre
!!$              csou_pre_farfar=csou_pre
!!$              dens_pre_farfar=dens_pre
!!$              geff_pre_farfar=geff_pre
!!$           endif
!!$#endif
!!$#if NDIM==2
!!$           x_pos=dble(i2)+x3(1)
!!$           x_pre=dble(i2)-x3(1)
!!$           y_pos=dble(i2)+x3(2)
!!$           y_pre=dble(i2)-x3(2)
!!$           if    (x_pos.le.4d0 .and. x_pos.ge.-1d0 .and. &
!!$                & y_pos.le.4d0 .and. y_pos.ge.-1d0)then
!!$              position(1:ndim)= gradT(i,i2,j2,k2,1:ndim)*nearlythree
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=pres(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              pres_pos_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=csou(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              csou_pos_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=dens(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              dens_pos_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=geff(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              geff_pos_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$           else
!!$              pres_pos_farfar=pres_pos
!!$              csou_pos_farfar=csou_pos
!!$              dens_pos_farfar=dens_pos
!!$              geff_pos_farfar=geff_pos
!!$           endif
!!$           if    (x_pre.le.4d0 .and. x_pre.ge.-1d0 .and. &
!!$                & y_pre.le.4d0 .and. y_pre.ge.-1d0)then
!!$              position(1:ndim)= gradT(i,i2,j2,k2,1:ndim)*nearlythree
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=pres(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              pres_pre_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=csou(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              csou_pre_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=dens(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              dens_pre_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=geff(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              geff_pre_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$           else
!!$              pres_pre_farfar=pres_pre
!!$              csou_pre_farfar=csou_pre
!!$              dens_pre_farfar=dens_pre
!!$              geff_pre_farfar=geff_pre
!!$           endif
!!$#endif
!!$#if NDIM==3
!!$           x_pos=dble(i2)+x3(1)
!!$           x_pre=dble(i2)-x3(1)
!!$           y_pos=dble(i2)+x3(2)
!!$           y_pre=dble(i2)-x3(2)
!!$           z_pos=dble(i2)+x3(3)
!!$           z_pre=dble(i2)-x3(3)
!!$           if    (x_pos.le.4d0 .and. x_pos.ge.-1d0 .and. &
!!$                & y_pos.le.4d0 .and. y_pos.ge.-1d0 .and. &
!!$                & z_pos.le.4d0 .and. z_pos.ge.-1d0)then
!!$              position(1:ndim)= gradT(i,i2,j2,k2,1:ndim)*nearlythree
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=pres(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              pres_pos_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=csou(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              csou_pos_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=dens(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              dens_pos_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=geff(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              geff_pos_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$           else
!!$              pres_pos_farfar=pres_pos
!!$              csou_pos_farfar=csou_pos
!!$              dens_pos_farfar=dens_pos
!!$              geff_pos_farfar=geff_pos
!!$           endif
!!$           if    (x_pre.le.4d0 .and. x_pre.ge.-1d0 .and. &
!!$                & y_pre.le.4d0 .and. y_pre.ge.-1d0 .and. &
!!$                & z_pre.le.4d0 .and. z_pre.ge.-1d0)then
!!$              position(1:ndim)= gradT(i,i2,j2,k2,1:ndim)*nearlythree
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=pres(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              pres_pre_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=csou(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              csou_pre_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=dens(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              dens_pre_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=geff(i,iu1:iu2,ju1:ju2,ku1:ku2)           
!!$              geff_pre_farfar=cic_interpol(position,quantity,i2,j2,k2)
!!$           else
!!$              pres_pre_farfar=pres_pre
!!$              csou_pre_farfar=csou_pre
!!$              dens_pre_farfar=dens_pre
!!$              geff_pre_farfar=geff_pre
!!$           endif
!!$#endif
!!$           if (pres_pre_farfar .lt. pres_pre)gamma_eff=geff_pre_farfar
!!$           pres_pre=MIN(pres_pre,pres_pre_farfar)
!!$           pres_pos=MAX(pres_pos,pres_pos_farfar)
!!$           csou_pre=MIN(csou_pre,csou_pre_farfar)
!!$           csou_pos=MAX(csou_pos,csou_pos_farfar)
!!$           dens_pre=MIN(dens_pre,dens_pre_farfar)
!!$           dens_pos=MAX(dens_pos,dens_pos_farfar)
!!$           ! ======= Try 3 cells ===================

           
           if(pres_pre.ne.0.0d0.and.pres_pos.ne.0.0d0)then
              pratio=MAX(pres_pos,pres(i,i2,j2,k2))/MIN(pres_pre,pres(i,i2,j2,k2))
           else
              pratio=1.0d0
           endif
           mach=sqrt( (gamma_eff+1d0)/(2d0*gamma_eff) * ( (gamma_eff-1d0)/(gamma_eff+1d0) + pratio) )
           mach2=mach**2
           Rcomp=(gamma_eff+1d0)*mach2/((gamma_eff-1d0)*mach2+2d0) ! Compression ratio

           if(smooth_shock)then
              machloc=mach
              mymax=machloc
#if NDIM==1
              do i3=-1,+1
                 if(machloc.gt.minmach.and.moldloc(i,i2+i3,j2,k2).gt.mymax)then
                    mymax=moldloc(i,i2+i3,j2,k2)
                    mach=0.5d0*(machloc+moldloc(i,i2+i3,j2,k2))
                    write(*,'(A,e12.4,i3,3e12.4)')'modif',t,i3,machloc,moldloc(i,i2+i3,j2   ,k2   ),mach
                 endif
              enddo
#endif
#if NDIM==2
              do j3=-1,+1
              do i3=-1,+1
                 if(machloc.gt.minmach.and.moldloc(i,i2+i3,j2+j3,k2).gt.mymax)then
                    mymax=moldloc(i,i2+i3,j2+j3,k2)
                    mach=0.5d0*(machloc+moldloc(i,i2+i3,j2+j3,k2))
                 endif
              enddo
              enddo
#endif
#if NDIM==3
              do k3=-1,+1
              do j3=-1,+1
              do i3=-1,+1
                 if(machloc.gt.minmach.and.moldloc(i,i2+i3,j2+j3,k2+k3).gt.mymax)then
                    mymax=moldloc(i,i2+i3,j2+j3,k2+k3)
                    mach=0.5d0*(machloc+moldloc(i,i2+i3,j2+j3,k2+k3))
                 endif
              enddo
              enddo
              enddo
#endif
           endif

           if(pres_pos.gt.pres_pos_far)then
              position(1:ndim)= gradT(i,i2,j2,k2,1:ndim)
           else
              position(1:ndim)= gradT(i,i2,j2,k2,1:ndim)*nearlytwo
           endif
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=ethe(i,iu1:iu2,ju1:ju2,ku1:ku2)
           eth_pos=cic_interpol(position,quantity,i2,j2,k2)
           ediss=eth_pos
#if NENER>0
           do irad=1,nener
              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=uloc(i,iu1:iu2,ju1:ju2,ku1:ku2,8+irad)
              ecr_pos(irad)=cic_interpol(position,quantity,i2,j2,k2)
!!$              ediss=ediss+ecr_pos(irad)
           enddo
#endif
           if(pres_pre.lt.pres_pre_far)then
              position(1:ndim)=-gradT(i,i2,j2,k2,1:ndim)
           else
              position(1:ndim)=-gradT(i,i2,j2,k2,1:ndim)*nearlytwo
           endif
           quantity(iu1:iu2,ju1:ju2,ku1:ku2)=ethe(i,iu1:iu2,ju1:ju2,ku1:ku2)
           eth_pre=cic_interpol(position,quantity,i2,j2,k2)
           ediss=ediss-eth_pre*Rcomp**gamma
#if NENER>0
!!$           do irad=1,nener
!!$              quantity(iu1:iu2,ju1:ju2,ku1:ku2)=uloc(i,iu1:iu2,ju1:ju2,ku1:ku2,8+irad)
!!$              ecr_pre(irad)=cic_interpol(position,quantity,i2,j2,k2)
!!$              ediss=ediss-ecr_pre(irad)*Rcomp**gamma_rad(irad)
!!$           enddo
#endif

           d_up  =MIN(dens(i,i2,j2,k2),dens_pre,dens_pos)
           csound=MIN(csou(i,i2,j2,k2),csou_pre,csou_pos)

           if(mach.gt.10.0d0)then
!!$              write(*,*)'mach=',mach,csound
              u_up=ABS(4d0*(uvel_pre-uvel_pos)/3d0)
!!$              write(*,'(A,pe10.3,x,pe10.3,x,pe10.3,x,pe10.3,x,pe10.3,x,pe10.3,x,pe10.3,x,pe10.3,x,pe10.3,x,pe10.3,x,pe10.3)') &
!!$                   &'u_up=',u_up,mach*csound,uvel_pre,uvel_pos,ediss,Rcomp &
!!$                   &,eta_mach*ediss*u_up/Rcomp*dtnew(ilevel)/dx_loc,0.5d0*enewloc(i,i2,j2,k2) &
!!$                   &,eth_pos,eth_pre*Rcomp**gamma,ecr_pos(1)
           else
              u_up=mach*csound
           endif
!!$           if(mach .gt.10.0d0)then 
!!$              u_up=MAX(ABS(4d0*(uvel_pre-uvel_pos)/3d0),mach*csound)
!!$           else
!!$              u_up=mach*csound
!!$           endif

!!$           cr_flux=0.5d0*eta_mach*d_up*(mach*csound)**3 ! Vazza+
!!$           cr_flux=eta_mach*ediss*mach*csound/Rcomp ! Pfrommer+
           cr_flux=eta_mach*ediss*u_up/Rcomp
           deltae=MIN(cr_flux*dtnew(ilevel)/dx_loc,0.5d0*enewloc(i,i2,j2,k2))
           
           ! Get cell index
           if(ndim==1)ind_son=1+(i2-1)
           if(ndim==2)ind_son=1+(i2-1)+2*(j2-1)
           if(ndim==3)ind_son=1+(i2-1)+2*(j2-1)+4*(k2-1)
           iskip=ncoarse+(ind_son-1)*ngridmax
           ind_cell(i)=iskip+ind_grid(i)
           if(mach.gt.minmach .and.okshock(i,i2,j2,k2).and.ediss.gt.0.0d0.and.t.gt.tmin_acc)then
!!$              write(*,'(A,9e13.5)')'mach(1)=',mach,csound,d_up,Rcomp&
!!$                   &,0.5d0*eta_mach*d_up*(mach*csound)**3&
!!$                   &,eta_mach*ediss*mach*csound/Rcomp&
!!$                   &,0.5d0*eta_mach*d_up*(mach*csound)**3*dtnew(ilevel)/dx_loc&
!!$                   &,eta_mach*ediss*mach*csound/Rcomp*dtnew(ilevel)/dx_loc&
!!$                   &,0.5d0*enewloc(i,i2,j2,k2)
!!$              write(*,'(A,6e13.5)')'mach(2)=',eth_pos,ecr_pos(1),eth_pre,ecr_pre(1),eth_pre*Rcomp**gamma,ecr_pre(1)*Rcomp**gamma_rad(1)
!!$              write(*,'(A,6e13.5)')'mach(3)=',d_up,csound,ediss,Rcomp&
!!$                   &,0.02*dx_loc*(ethe(i,i2,j2,k2)+uloc(i,i2,j2,k2,8+irad))/cr_flux,dtnew(ilevel)
              ! The MAX is to avoid negative values in temperature
              unew(ind_cell(i),ind_cr+1)=unew(ind_cell(i),ind_cr+1)+deltae
#if NVAR>8+NENER
              unew(ind_cell(i),ind_mach)=mach*MAX(unew(ind_cell(i),1),smallr)
#endif
           endif
#if NVAR>8+NENER
!!$           unew(ind_cell(i),10+nener)=mach*max(unew(ind_cell(i),1),smallr)
!!$           unew(ind_cell(i),11+nener)=divu(ind_cell(i))*max(unew(ind_cell(i),1),smallr)
!!$           do idim=1,ndim
!!$              unew(ind_cell(i),11+nener+idim)=-gradT(i,i2,j2,k2,idim)*max(unew(ind_cell(i),1),smallr)
!!$           enddo
!!$           unew(ind_cell(i),11+nener+ndim+1)=pres_pre*max(unew(ind_cell(i),1),smallr)
!!$           unew(ind_cell(i),11+nener+ndim+2)=pres_pos*max(unew(ind_cell(i),1),smallr)
#endif

        endif
     endif
     enddo
  enddo
  enddo
  enddo

111 format('   Entering add_pcr_shock_source_terms for level ',i2)

end subroutine add_pcr_shock_source_terms
!###########################################################
!###########################################################
!###########################################################
!###########################################################
function cic_interpol(position,quantity,i2,j2,k2)
  use amr_commons
  use hydro_commons
  implicit none
  real(dp),dimension(1:ndim)::position
  real(dp),dimension(1:ndim)::x,dd,dg
  real(dp),dimension(iu1:iu2,ju1:ju2,ku1:ku2)::quantity
  real(dp),dimension(1:twotondim),save::vol

  integer,dimension(1:ndim)::itarget
  integer,dimension(1:ndim)::iig,iid
  integer,dimension(1:twotondim)::icic,jcic,kcic

  integer::idim,icell,i2,j2,k2

  real(dp)::cic_interpol

  cic_interpol=0.0d0

  itarget(1:ndim)=nint(position(1:ndim))
  x(1:ndim)=position(1:ndim)-dble(itarget(1:ndim))+0.5D0 ! Rescale position to the cell interval [0,1]
  do idim=1,ndim
     dd(idim)=x(idim)+0.5D0
     iid(idim)=dd(idim)
     dd(idim)=dd(idim)-iid(idim)
     dg(idim)=1.0D0-dd(idim)
     iig(idim)=iid(idim)-1
  enddo
#if NDIM==1
  icic(1)=iig(1)+itarget(1)
  icic(2)=iid(1)+itarget(1)
  vol(1)=dg(1)
  vol(2)=dd(1)
  do icell=1,twotondim
     cic_interpol=cic_interpol+vol(icell)*quantity(i2+icic(icell),j2,k2)
  enddo
#endif
#if NDIM==2
  icic(1)=iig(1)+itarget(1)
  icic(2)=iid(1)+itarget(1)
  icic(3)=iig(1)+itarget(1)
  icic(4)=iid(1)+itarget(1)
  jcic(1)=iig(2)+itarget(2)
  jcic(2)=iig(2)+itarget(2)
  jcic(3)=iid(2)+itarget(2)
  jcic(4)=iid(2)+itarget(2)
  vol(1)=dg(1)*dg(2)
  vol(2)=dd(1)*dg(2)
  vol(3)=dg(1)*dd(2)
  vol(4)=dd(1)*dd(2)
  do icell=1,twotondim
     cic_interpol=cic_interpol+vol(icell)*quantity(i2+icic(icell),j2+jcic(icell),k2)
  enddo
#endif
#if NDIM==3
  icic(1)=iig(1)+itarget(1)
  icic(2)=iid(1)+itarget(1)
  icic(3)=iig(1)+itarget(1)
  icic(4)=iid(1)+itarget(1)
  icic(5)=iig(1)+itarget(1)
  icic(6)=iid(1)+itarget(1)
  icic(7)=iig(1)+itarget(1)
  icic(8)=iid(1)+itarget(1)
  jcic(1)=iig(2)+itarget(2)
  jcic(2)=iig(2)+itarget(2)
  jcic(3)=iid(2)+itarget(2)
  jcic(4)=iid(2)+itarget(2)
  jcic(5)=iig(2)+itarget(2)
  jcic(6)=iig(2)+itarget(2)
  jcic(7)=iid(2)+itarget(2)
  jcic(8)=iid(2)+itarget(2)
  kcic(1)=iig(3)+itarget(3)
  kcic(2)=iig(3)+itarget(3)
  kcic(3)=iig(3)+itarget(3)
  kcic(4)=iig(3)+itarget(3)
  kcic(5)=iid(3)+itarget(3)
  kcic(6)=iid(3)+itarget(3)
  kcic(7)=iid(3)+itarget(3)
  kcic(8)=iid(3)+itarget(3)
  vol(1)=dg(1)*dg(2)*dg(3)
  vol(2)=dd(1)*dg(2)*dg(3)
  vol(3)=dg(1)*dd(2)*dg(3)
  vol(4)=dd(1)*dd(2)*dg(3)
  vol(5)=dg(1)*dg(2)*dd(3)
  vol(6)=dd(1)*dg(2)*dd(3)
  vol(7)=dg(1)*dd(2)*dd(3)
  vol(8)=dd(1)*dd(2)*dd(3)
  do icell=1,twotondim
     cic_interpol=cic_interpol+vol(icell)*quantity(i2+icic(icell),j2+jcic(icell),k2+kcic(icell))
  enddo
#endif

end function cic_interpol
!###########################################################
!###########################################################
!###########################################################
!###########################################################
subroutine godfine1(ind_grid,ncache,ilevel)
  use amr_commons
  use hydro_commons
  use poisson_commons
  implicit none
  integer::ilevel,ncache
  integer,dimension(1:nvector)::ind_grid
  !-------------------------------------------------------------------
  ! This routine gathers first hydro variables from neighboring grids
  ! to set initial conditions in a 6x6x6 grid. It interpolate from
  ! coarser level missing grid variables. It then calls the
  ! Godunov solver that computes fluxes. These fluxes are zeroed at 
  ! coarse-fine boundaries, since contribution from finer levels has
  ! already been taken into account. Conservative variables are updated 
  ! and stored in array unew(:), both at the current level and at the 
  ! coarser level if necessary.
  !-------------------------------------------------------------------
  integer ,dimension(1:nvector,1:threetondim     ),save::nbors_father_cells
  integer ,dimension(1:nvector,1:twotondim       ),save::nbors_father_grids
  integer ,dimension(1:nvector,0:twondim         ),save::ibuffer_father
  integer ,dimension(1:nvector,0:twondim         ),save::ind1
  real(dp),dimension(1:nvector,0:twondim  ,1:nvar+3),save::u1
  real(dp),dimension(1:nvector,1:twotondim,1:nvar+3),save::u2
  real(dp),dimension(1:nvector,0:twondim  ,1:ndim),save::g1=0.0d0
  real(dp),dimension(1:nvector,1:twotondim,1:ndim),save::g2=0.0d0

  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar+3),save::uloc
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:ndim),save::gloc=0.0d0
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2,1:nvar,1:ndim),save::flux
  real(dp),dimension(1:nvector,1:3,1:3,1:3),save::emfx=0.0d0,emfy=0.0d0,emfz=0.0d0
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2,1:2,1:ndim),save::tmp
  logical ,dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2),save::ok

  integer,dimension(1:nvector),save::igrid_nbor,ind_cell,ind_buffer,ind_exist,ind_nexist

  integer::neul=5
  integer::ind_buffer1,ind_buffer2,ind_buffer3
  integer::ind_father1,ind_father2,ind_father3
  integer::i,j,ivar,idim,ind_son,ind_father,iskip,nbuffer,ibuffer
  integer::i0,j0,k0,i1,j1,k1,i2,j2,k2,i3,j3,k3,nx_loc,nb_noneigh,nexist
  integer::i1min,i1max,j1min,j1max,k1min,k1max
  integer::i2min,i2max,j2min,j2max,k2min,k2max
  integer::i3min,i3max,j3min,j3max,k3min,k3max
  real(dp)::dflux_x,dflux_y,dflux_z
  real(dp)::dx,scale,oneontwotondim
  real(dp)::dflux,weight

  oneontwotondim = 1.d0/dble(twotondim)

  ! Mesh spacing in that level
  nx_loc=icoarse_max-icoarse_min+1
  scale=boxlen/dble(nx_loc)
  dx=0.5D0**ilevel*scale

  ! Integer constants
  i1min=0; i1max=0; i2min=0; i2max=0; i3min=1; i3max=1
  j1min=0; j1max=0; j2min=0; j2max=0; j3min=1; j3max=1
  k1min=0; k1max=0; k2min=0; k2max=0; k3min=1; k3max=1
  if(ndim>0)then
     i1max=2; i2max=1; i3max=2
  end if
  if(ndim>1)then
     j1max=2; j2max=1; j3max=2
  end if
  if(ndim>2)then
     k1max=2; k2max=1; k3max=2
  end if

  !------------------------------------------
  ! Gather 3^ndim neighboring father cells
  !------------------------------------------
  do i=1,ncache
     ind_cell(i)=father(ind_grid(i))
  end do
  call get3cubefather(ind_cell,nbors_father_cells,nbors_father_grids,ncache,ilevel)
  
  !---------------------------
  ! Gather 6x6x6 cells stencil
  !---------------------------
  ! Loop over 3x3x3 neighboring father cells
  do k1=k1min,k1max
  do j1=j1min,j1max
  do i1=i1min,i1max
     
     ! Check if neighboring grid exists
     nbuffer=0
     nexist=0
     ind_father=1+i1+3*j1+9*k1
     do i=1,ncache
        igrid_nbor(i)=son(nbors_father_cells(i,ind_father))
        if(igrid_nbor(i)>0) then
           nexist=nexist+1
           ind_exist(nexist)=i
        else
          nbuffer=nbuffer+1
          ind_nexist(nbuffer)=i
          ind_buffer(nbuffer)=nbors_father_cells(i,ind_father)
        end if
     end do
     
     ! If not, interpolate variables from parent cells
     if(nbuffer>0)then
        call getnborfather(ind_buffer,ibuffer_father,nbuffer,ilevel)
        do j=0,twondim
           do ivar=1,nvar+3
              do i=1,nbuffer
                 u1(i,j,ivar)=uold(ibuffer_father(i,j),ivar)
              end do
           end do
           do i=1,nbuffer
              ind1(i,j)=son(ibuffer_father(i,j))
           end do
        end do
        call interpol_hydro(u1,ind1,u2,nbuffer)
     endif

     ! Loop over 2x2x2 cells
     do k2=k2min,k2max
     do j2=j2min,j2max
     do i2=i2min,i2max

        ind_son=1+i2+2*j2+4*k2
        iskip=ncoarse+(ind_son-1)*ngridmax
        do i=1,nexist
           ind_cell(i)=iskip+igrid_nbor(ind_exist(i))
        end do
        
        i3=1; j3=1; k3=1
        if(ndim>0)i3=1+2*(i1-1)+i2
        if(ndim>1)j3=1+2*(j1-1)+j2
        if(ndim>2)k3=1+2*(k1-1)+k2
        
        ! Gather hydro variables
        do ivar=1,nvar+3
           do i=1,nexist
              uloc(ind_exist(i),i3,j3,k3,ivar)=uold(ind_cell(i),ivar)
           end do
           do i=1,nbuffer
              uloc(ind_nexist(i),i3,j3,k3,ivar)=u2(i,ind_son,ivar)
           end do
        end do
        
        ! Gather gravitational acceleration
        if(poisson)then
           do idim=1,ndim
              do i=1,nexist
                 gloc(ind_exist(i),i3,j3,k3,idim)=f(ind_cell(i),idim)
              end do
              do i=1,nbuffer
                 gloc(ind_nexist(i),i3,j3,k3,idim)=f(ibuffer_father(i,0),idim)
              end do
           end do
        end if
        
        ! Gather refinement flag
        do i=1,nexist
           ok(ind_exist(i),i3,j3,k3)=son(ind_cell(i))>0
        end do
        do i=1,nbuffer
           ok(ind_nexist(i),i3,j3,k3)=.false.
        end do
        
     end do
     end do
     end do
     ! End loop over cells

  end do
  end do
  end do
  ! End loop over neighboring grids

  !-----------------------------------------------
  ! Compute flux using second-order Godunov method
  !-----------------------------------------------
  call mag_unsplit(uloc,gloc,flux,emfx,emfy,emfz,tmp,dx,dx,dx,dtnew(ilevel),ncache)

  if(ischeme.eq.1)then
  !---------------------------------
  ! Reset all Euler variables fluxes
  !---------------------------------
  do idim=1,ndim
  do ivar=1,neul
     do k3=k3min,k3max+1
     do j3=j3min,j3max+1
     do i3=i3min,i3max+1
        do i=1,ncache
           flux(i,i3,j3,k3,ivar,idim)=0.0d0
        end do
     end do
     end do
     end do
  end do
  end do

  endif

  !------------------------------------------------
  ! Reset flux along direction at refined interface    
  !------------------------------------------------
  do idim=1,ndim
     i0=0; j0=0; k0=0
     if(idim==1)i0=1
     if(idim==2)j0=1
     if(idim==3)k0=1
     do k3=k3min,k3max+k0
     do j3=j3min,j3max+j0
     do i3=i3min,i3max+i0
        do ivar=1,nvar
           do i=1,ncache
              if(ok(i,i3-i0,j3-j0,k3-k0) .or. ok(i,i3,j3,k3))then
                 flux(i,i3,j3,k3,ivar,idim)=0.0d0
              end if
           end do
        end do
        if(pressure_fix)then
        do ivar=1,2
           do i=1,ncache
              if(ok(i,i3-i0,j3-j0,k3-k0) .or. ok(i,i3,j3,k3))then
                 tmp (i,i3,j3,k3,ivar,idim)=0.0d0
              end if
           end do
        end do
        end if
     end do
     end do
     end do
  end do
  !---------------------------------------------------------------
  ! Reset Euler fluxes for Bx
  !---------------------------------------------------------------
  do idim=1,ndim
     i0=0; j0=0; k0=0
     if(idim==1)i0=1
     if(idim==2)j0=1
     if(idim==3)k0=1
     do k3=k3min,k3max+k0
     do j3=j3min,j3max+j0
     do i3=i3min,i3max+i0
        do i=1,ncache
           flux(i,i3,j3,k3,6,idim)=0.0d0
        end do
     end do
     end do
     end do
  end do
#if NDIM>1
  !---------------------------------------------------------------
  ! Reset electromotive force along direction z at refined edges
  !---------------------------------------------------------------
  do k3=k3min,k3max
  do j3=1,3
  do i3=1,3
     do i=1,ncache
        if(ok(i,i3  ,j3  ,k3) .or. ok(i,i3  ,j3-1,k3) .or.  &
         & ok(i,i3-1,j3  ,k3) .or. ok(i,i3-1,j3-1,k3))then
           emfz(i,i3,j3,k3)=0.0d0
#if NDIM==2
           emfz(i,i3,j3,k3+1)=0.0d0
#endif
        end if
     end do
  end do
  end do
  end do
  !---------------------------------------------------------------
  ! Reset Euler fluxes for By
  !---------------------------------------------------------------
  do idim=1,ndim
     i0=0; j0=0; k0=0
     if(idim==1)i0=1
     if(idim==2)j0=1
     if(idim==3)k0=1
     do k3=k3min,k3max+k0
     do j3=j3min,j3max+j0
     do i3=i3min,i3max+i0
        do i=1,ncache
           flux(i,i3,j3,k3,7,idim)=0.0d0
        end do
     end do
     end do
     end do
  end do
#endif
#if NDIM>2
  !---------------------------------------------------------------
  ! Reset electromotive force along direction y at refined edges
  !---------------------------------------------------------------
  do k3=1,3
  do j3=1,2
  do i3=1,3
     do i=1,ncache
        if(ok(i,i3  ,j3,k3  ) .or. ok(i,i3  ,j3,k3-1) .or.  &
         & ok(i,i3-1,j3,k3  ) .or. ok(i,i3-1,j3,k3-1))then
           emfy(i,i3,j3,k3)=0.0d0
        end if
     end do
  end do
  end do
  end do
  !---------------------------------------------------------------
  ! Reset electromotive force along direction x at refined edges
  !---------------------------------------------------------------
  do k3=1,3
  do j3=1,3
  do i3=1,2
     do i=1,ncache
        if(ok(i,i3,j3  ,k3  ) .or. ok(i,i3,j3  ,k3-1) .or.  &
         & ok(i,i3,j3-1,k3  ) .or. ok(i,i3,j3-1,k3-1))then
           emfx(i,i3,j3,k3)=0.0d0
        end if
     end do
  end do
  end do
  end do
  !---------------------------------------------------------------
  ! Reset Euler fluxes for Bz
  !---------------------------------------------------------------
  do idim=1,ndim
     i0=0; j0=0; k0=0
     if(idim==1)i0=1
     if(idim==2)j0=1
     if(idim==3)k0=1
     do k3=k3min,k3max+k0
     do j3=j3min,j3max+j0
     do i3=i3min,i3max+i0
        do i=1,ncache
           flux(i,i3,j3,k3,8,idim)=0.0d0
        end do
     end do
     end do
     end do
  end do
#endif

  !-----------------------------------------------------
  ! Conservative update at level ilevel for Euler system
  !-----------------------------------------------------
  do idim=1,ndim
     i0=0; j0=0; k0=0
     if(idim==1)i0=1
     if(idim==2)j0=1
     if(idim==3)k0=1
     do k2=k2min,k2max
     do j2=j2min,j2max
     do i2=i2min,i2max
        ind_son=1+i2+2*j2+4*k2
        iskip=ncoarse+(ind_son-1)*ngridmax
        do i=1,ncache
           ind_cell(i)=iskip+ind_grid(i)
        end do
        i3=1+i2
        j3=1+j2
        k3=1+k2
        ! Update conservative variables new state vector
        do ivar=1,nvar
           do i=1,ncache
              unew(ind_cell(i),ivar)=unew(ind_cell(i),ivar)+ &
                   & (flux(i,i3   ,j3   ,k3   ,ivar,idim) &
                   & -flux(i,i3+i0,j3+j0,k3+k0,ivar,idim))
           end do
        end do
        do ivar=1,3
           do i=1,ncache
              unew(ind_cell(i),nvar+ivar)=unew(ind_cell(i),nvar+ivar)+ &
                   & (flux(i,i3   ,j3   ,k3   ,neul+ivar,idim) &
                   & -flux(i,i3+i0,j3+j0,k3+k0,neul+ivar,idim))
           end do
        end do

        if(pressure_fix)then
        ! Update velocity divergence
        do i=1,ncache
           divu(ind_cell(i))=divu(ind_cell(i))+ &
                & (tmp(i,i3   ,j3   ,k3   ,1,idim) &
                & -tmp(i,i3+i0,j3+j0,k3+k0,1,idim))
        end do
        ! Update internal energy
        do i=1,ncache
           enew(ind_cell(i))=enew(ind_cell(i))+ &
                & (tmp(i,i3   ,j3   ,k3   ,2,idim) &
                & -tmp(i,i3+i0,j3+j0,k3+k0,2,idim))
        end do
        end if
     end do
     end do
     end do
  end do

  !---------------------------------------------------------
  ! Conservative update at level ilevel for induction system
  !---------------------------------------------------------
  do k3=k3min,k3max
  do j3=j3min,j3max
  do i3=1,2
     ind_son=i3+2*(j3-1)+4*(k3-1)
     iskip=ncoarse+(ind_son-1)*ngridmax
     do i=1,ncache
        ind_cell(i)=iskip+ind_grid(i)
     end do
     ! Update Bx using constraint transport
     do i=1,ncache
        dflux_x=( emfy(i,i3,j3,k3)-emfy(i,i3,j3,k3+1) ) &
          &    -( emfz(i,i3,j3,k3)-emfz(i,i3,j3+1,k3) )
        unew(ind_cell(i),neul+1)=unew(ind_cell(i),neul+1)+dflux_x
        dflux_x=( emfy(i,i3+1,j3,k3)-emfy(i,i3+1,j3,k3+1) ) &
          &    -( emfz(i,i3+1,j3,k3)-emfz(i,i3+1,j3+1,k3) )  
        unew(ind_cell(i),nvar+1)=unew(ind_cell(i),nvar+1)+dflux_x
     end do
  end do
  end do
  end do
#if NDIM>1  
  do k3=k3min,k3max
  do j3=1,2
  do i3=1,2
     ind_son=i3+2*(j3-1)+4*(k3-1)
     iskip=ncoarse+(ind_son-1)*ngridmax
     do i=1,ncache
        ind_cell(i)=iskip+ind_grid(i)
     end do
     ! Update By using constraint transport
     do i=1,ncache
        dflux_y=( emfz(i,i3,j3,k3)-emfz(i,i3+1,j3,k3) ) &
          &    -( emfx(i,i3,j3,k3)-emfx(i,i3,j3,k3+1) )
        unew(ind_cell(i),neul+2)=unew(ind_cell(i),neul+2)+dflux_y
        dflux_y=( emfz(i,i3,j3+1,k3)-emfz(i,i3+1,j3+1,k3) ) &
          &    -( emfx(i,i3,j3+1,k3)-emfx(i,i3,j3+1,k3+1) )
        unew(ind_cell(i),nvar+2)=unew(ind_cell(i),nvar+2)+dflux_y
     end do
  end do
  end do
  end do
#endif
#if NDIM>2
  do k3=1,2
  do j3=1,2
  do i3=1,2
     ind_son=i3+2*(j3-1)+4*(k3-1)
     iskip=ncoarse+(ind_son-1)*ngridmax
     do i=1,ncache
        ind_cell(i)=iskip+ind_grid(i)
     end do
     ! Update Bz using constraint transport
     do i=1,ncache
        dflux_z=( emfx(i,i3,j3,k3)-emfx(i,i3,j3+1,k3) ) &
          &    -( emfy(i,i3,j3,k3)-emfy(i,i3+1,j3,k3) )
        unew(ind_cell(i),neul+3)=unew(ind_cell(i),neul+3)+dflux_z
        dflux_z=( emfx(i,i3,j3,k3+1)-emfx(i,i3,j3+1,k3+1) ) &
          &    -( emfy(i,i3,j3,k3+1)-emfy(i,i3+1,j3,k3+1) )
        unew(ind_cell(i),nvar+3)=unew(ind_cell(i),nvar+3)+dflux_z
     end do
  end do
  end do
  end do
#endif

  if(ilevel>levelmin)then

  !-----------------------------------------------------------
  ! Conservative update at level ilevel-1 for the Euler system
  !-----------------------------------------------------------
  ! Loop over dimensions
  do idim=1,ndim
     i0=0; j0=0; k0=0
     if(idim==1)i0=1
     if(idim==2)j0=1
     if(idim==3)k0=1
     
     !----------------------
     ! Left flux at boundary
     !----------------------     
     ! Check if grids sits near left boundary
     ! and gather neighbor father cells index
     nb_noneigh=0
     do i=1,ncache
        if (son(nbor(ind_grid(i),2*idim-1))==0) then
           nb_noneigh = nb_noneigh + 1
           ind_buffer(nb_noneigh) = nbor(ind_grid(i),2*idim-1)
           ind_cell(nb_noneigh) = i
        end if
     end do
     ! Conservative update of new state variables
     do ivar=1,nvar
        ! Loop over boundary cells
        do k3=k3min,k3max-k0
        do j3=j3min,j3max-j0
        do i3=i3min,i3max-i0
           do i=1,nb_noneigh
              unew(ind_buffer(i),ivar)=unew(ind_buffer(i),ivar) &
                   & -flux(ind_cell(i),i3,j3,k3,ivar,idim)*oneontwotondim
           end do
        end do
        end do
        end do
     end do
     do ivar=1,3
        ! Loop over boundary cells
        do k3=k3min,k3max-k0
        do j3=j3min,j3max-j0
        do i3=i3min,i3max-i0
           do i=1,nb_noneigh
              unew(ind_buffer(i),nvar+ivar)=unew(ind_buffer(i),nvar+ivar) &
                   & -flux(ind_cell(i),i3,j3,k3,neul+ivar,idim)*oneontwotondim
           end do
        end do
        end do
        end do
     end do
     if(pressure_fix)then
     ! Update velocity divergence
     do k3=k3min,k3max-k0
     do j3=j3min,j3max-j0
     do i3=i3min,i3max-i0
        do i=1,nb_noneigh
           divu(ind_buffer(i))=divu(ind_buffer(i)) &
                & -tmp(ind_cell(i),i3,j3,k3,1,idim)*oneontwotondim
        end do
     end do
     end do
     end do
     ! Update internal energy
     do k3=k3min,k3max-k0
     do j3=j3min,j3max-j0
     do i3=i3min,i3max-i0
        do i=1,nb_noneigh
           enew(ind_buffer(i))=enew(ind_buffer(i)) &
                & -tmp(ind_cell(i),i3,j3,k3,2,idim)*oneontwotondim
        end do
     end do
     end do
     end do
     end if
     
     !-----------------------
     ! Right flux at boundary
     !-----------------------     
     ! Check if grids sits near right boundary
     ! and gather neighbor father cells index
     nb_noneigh=0
     do i=1,ncache
        if (son(nbor(ind_grid(i),2*idim))==0) then
           nb_noneigh = nb_noneigh + 1
           ind_buffer(nb_noneigh) = nbor(ind_grid(i),2*idim)
           ind_cell(nb_noneigh) = i
        end if
     end do
     ! Conservative update of new state variables
     do ivar=1,nvar
        ! Loop over boundary cells
        do k3=k3min+k0,k3max
        do j3=j3min+j0,j3max
        do i3=i3min+i0,i3max
           do i=1,nb_noneigh
              unew(ind_buffer(i),ivar)=unew(ind_buffer(i),ivar) &
                   & +flux(ind_cell(i),i3+i0,j3+j0,k3+k0,ivar,idim)*oneontwotondim
           end do
        end do
        end do
        end do
     end do
     do ivar=1,3
        ! Loop over boundary cells
        do k3=k3min+k0,k3max
        do j3=j3min+j0,j3max
        do i3=i3min+i0,i3max
           do i=1,nb_noneigh
              unew(ind_buffer(i),nvar+ivar)=unew(ind_buffer(i),nvar+ivar) &
                   & +flux(ind_cell(i),i3+i0,j3+j0,k3+k0,neul+ivar,idim)*oneontwotondim
           end do
        end do
        end do
        end do
     end do
     if(pressure_fix)then
     ! Update velocity divergence
     do k3=k3min+k0,k3max
     do j3=j3min+j0,j3max
     do i3=i3min+i0,i3max
        do i=1,nb_noneigh
           divu(ind_buffer(i))=divu(ind_buffer(i)) &
                & +tmp(ind_cell(i),i3+i0,j3+j0,k3+k0,1,idim)*oneontwotondim
        end do
     end do
     end do
     end do
     ! Update internal energy
     do k3=k3min+k0,k3max
     do j3=j3min+j0,j3max
     do i3=i3min+i0,i3max
        do i=1,nb_noneigh
           enew(ind_buffer(i))=enew(ind_buffer(i)) &
                & +tmp(ind_cell(i),i3+i0,j3+j0,k3+k0,2,idim)*oneontwotondim
        end do
     end do
     end do
     end do
     end if

  end do
  ! End loop over dimensions

#if NDIM>1  
  !---------------------------------------------------------------
  ! Conservative update at level ilevel-1 for the induction system
  !---------------------------------------------------------------
  i1=1; j1=0; k1=0
  if(ndim>1)j1=1
  if(ndim>2)k1=1

  !--------------------------------------
  ! Deal with 4 EMFz edges
  !--------------------------------------

  ! Update coarse Bx and By using fine EMFz on X=0 and Y=0 grid edge
  ind_father1=1+(i1  )+3*(j1-1)+9*(k1  )
  ind_father2=1+(i1-1)+3*(j1-1)+9*(k1  )
  ind_father3=1+(i1-1)+3*(j1  )+9*(k1  )
  do i=1,ncache
     ind_buffer1=nbors_father_cells(i,ind_father1)
     ind_buffer2=nbors_father_cells(i,ind_father2)
     ind_buffer3=nbors_father_cells(i,ind_father3)
     weight=1.0
     if(son(ind_buffer1)>0.and.son(ind_buffer3)>0) cycle
     if(son(ind_buffer1)>0.or.son(ind_buffer2)>0.or.son(ind_buffer3)>0)weight=0.5
     dflux=(emfz(i,1,1,1)+emfz(i,1,1,2))*0.25*weight
     unew(ind_buffer1,1+neul)=unew(ind_buffer1,1+neul)+dflux
     unew(ind_buffer2,1+nvar)=unew(ind_buffer2,1+nvar)+dflux
     unew(ind_buffer2,2+nvar)=unew(ind_buffer2,2+nvar)-dflux
     unew(ind_buffer3,2+neul)=unew(ind_buffer3,2+neul)-dflux
     if(son(ind_buffer1)==0.and.son(ind_buffer2)==0.and.son(ind_buffer3)==0) then
        unew(ind_buffer3,1+nvar)=unew(ind_buffer3,1+nvar)-dflux*0.5
        unew(ind_buffer1,2+nvar)=unew(ind_buffer1,2+nvar)+dflux*0.5
     endif
  end do

  ! Update coarse Bx and By using fine EMFz on X=0 and Y=1 grid edge
  ind_father1=1+(i1-1)+3*(j1  )+9*(k1  )
  ind_father2=1+(i1-1)+3*(j1+1)+9*(k1  )
  ind_father3=1+(i1  )+3*(j1+1)+9*(k1  )
  do i=1,ncache
     ind_buffer1=nbors_father_cells(i,ind_father1)
     ind_buffer2=nbors_father_cells(i,ind_father2)
     ind_buffer3=nbors_father_cells(i,ind_father3)
     weight=1.0
     if(son(ind_buffer1)>0.and.son(ind_buffer3)>0) cycle
     if(son(ind_buffer1)>0.or.son(ind_buffer2)>0.or.son(ind_buffer3)>0)weight=0.5
     dflux=(emfz(i,1,3,1)+emfz(i,1,3,2))*0.25*weight
     unew(ind_buffer1,2+nvar)=unew(ind_buffer1,2+nvar)-dflux
     unew(ind_buffer2,2+neul)=unew(ind_buffer2,2+neul)-dflux
     unew(ind_buffer2,1+nvar)=unew(ind_buffer2,1+nvar)-dflux
     unew(ind_buffer3,1+neul)=unew(ind_buffer3,1+neul)-dflux
     if(son(ind_buffer1)==0.and.son(ind_buffer2)==0.and.son(ind_buffer3)==0) then
        unew(ind_buffer3,2+neul)=unew(ind_buffer3,2+neul)+dflux*0.5
        unew(ind_buffer1,1+nvar)=unew(ind_buffer1,1+nvar)+dflux*0.5
     endif
  end do

  ! Update coarse Bx and By using fine EMFz on X=1 and Y=1 grid edge
  ind_father1=1+(i1  )+3*(j1+1)+9*(k1  )
  ind_father2=1+(i1+1)+3*(j1+1)+9*(k1  )
  ind_father3=1+(i1+1)+3*(j1  )+9*(k1  )
  do i=1,ncache
     ind_buffer1=nbors_father_cells(i,ind_father1)
     ind_buffer2=nbors_father_cells(i,ind_father2)
     ind_buffer3=nbors_father_cells(i,ind_father3)
     weight=1.0
     if(son(ind_buffer1)>0.and.son(ind_buffer3)>0) cycle
     if(son(ind_buffer1)>0.or.son(ind_buffer2)>0.or.son(ind_buffer3)>0)weight=0.5
     dflux=(emfz(i,3,3,1)+emfz(i,3,3,2))*0.25*weight
     unew(ind_buffer1,1+nvar)=unew(ind_buffer1,1+nvar)-dflux
     unew(ind_buffer2,1+neul)=unew(ind_buffer2,1+neul)-dflux
     unew(ind_buffer2,2+neul)=unew(ind_buffer2,2+neul)+dflux
     unew(ind_buffer3,2+nvar)=unew(ind_buffer3,2+nvar)+dflux
     if(son(ind_buffer1)==0.and.son(ind_buffer2)==0.and.son(ind_buffer3)==0) then
        unew(ind_buffer3,1+neul)=unew(ind_buffer3,1+neul)+dflux*0.5
        unew(ind_buffer1,2+neul)=unew(ind_buffer1,2+neul)-dflux*0.5
     endif
  end do

  ! Update coarse Bx and By using fine EMFz on X=1 and Y=0 grid edge
  ind_father1=1+(i1+1)+3*(j1  )+9*(k1  )
  ind_father2=1+(i1+1)+3*(j1-1)+9*(k1  )
  ind_father3=1+(i1  )+3*(j1-1)+9*(k1  )
  do i=1,ncache
     ind_buffer1=nbors_father_cells(i,ind_father1)
     ind_buffer2=nbors_father_cells(i,ind_father2)
     ind_buffer3=nbors_father_cells(i,ind_father3)
     weight=1.0
     if(son(ind_buffer1)>0.and.son(ind_buffer3)>0) cycle
     if(son(ind_buffer1)>0.or.son(ind_buffer2)>0.or.son(ind_buffer3)>0)weight=0.5
     dflux=(emfz(i,3,1,1)+emfz(i,3,1,2))*0.25*weight
     unew(ind_buffer1,2+neul)=unew(ind_buffer1,2+neul)+dflux
     unew(ind_buffer2,2+nvar)=unew(ind_buffer2,2+nvar)+dflux
     unew(ind_buffer2,1+neul)=unew(ind_buffer2,1+neul)+dflux
     unew(ind_buffer3,1+nvar)=unew(ind_buffer3,1+nvar)+dflux
     if(son(ind_buffer1)==0.and.son(ind_buffer2)==0.and.son(ind_buffer3)==0) then
        unew(ind_buffer3,2+nvar)=unew(ind_buffer3,2+nvar)-dflux*0.5
        unew(ind_buffer1,1+neul)=unew(ind_buffer1,1+neul)-dflux*0.5
     endif
  end do
#endif
  !--------------------------------------
  ! Deal with 4 EMFx edges
  !--------------------------------------
#if NDIM>2
  ! Update coarse By and Bz using fine EMFx on Y=0 and Z=0 grid edge
  ind_father1=1+(i1  )+3*(j1  )+9*(k1-1)
  ind_father2=1+(i1  )+3*(j1-1)+9*(k1-1)
  ind_father3=1+(i1  )+3*(j1-1)+9*(k1  )
  do i=1,ncache
     ind_buffer1=nbors_father_cells(i,ind_father1)
     ind_buffer2=nbors_father_cells(i,ind_father2)
     ind_buffer3=nbors_father_cells(i,ind_father3)
     weight=1.0
     if(son(ind_buffer1)>0.and.son(ind_buffer3)>0) cycle
     if(son(ind_buffer1)>0.or.son(ind_buffer2)>0.or.son(ind_buffer3)>0)weight=0.5
     dflux=(emfx(i,1,1,1)+emfx(i,2,1,1))*0.25*weight
     unew(ind_buffer1,2+neul)=unew(ind_buffer1,2+neul)+dflux
     unew(ind_buffer2,2+nvar)=unew(ind_buffer2,2+nvar)+dflux
     unew(ind_buffer2,3+nvar)=unew(ind_buffer2,3+nvar)-dflux
     unew(ind_buffer3,3+neul)=unew(ind_buffer3,3+neul)-dflux
     if(son(ind_buffer1)==0.and.son(ind_buffer2)==0.and.son(ind_buffer3)==0) then
        unew(ind_buffer1,3+nvar)=unew(ind_buffer1,3+nvar)+dflux*0.5
        unew(ind_buffer3,2+nvar)=unew(ind_buffer3,2+nvar)-dflux*0.5
     endif
  end do

  ! Update coarse By and Bz using fine EMFx on Y=0 and Z=1 grid edge
  ind_father1=1+(i1  )+3*(j1-1)+9*(k1  )
  ind_father2=1+(i1  )+3*(j1-1)+9*(k1+1)
  ind_father3=1+(i1  )+3*(j1  )+9*(k1+1)
  do i=1,ncache
     ind_buffer1=nbors_father_cells(i,ind_father1)
     ind_buffer2=nbors_father_cells(i,ind_father2)
     ind_buffer3=nbors_father_cells(i,ind_father3)
     weight=1.0
     if(son(ind_buffer1)>0.and.son(ind_buffer3)>0) cycle
     if(son(ind_buffer1)>0.or.son(ind_buffer2)>0.or.son(ind_buffer3)>0)weight=0.5
     dflux=(emfx(i,1,1,3)+emfx(i,2,1,3))*0.25*weight
     unew(ind_buffer1,3+nvar)=unew(ind_buffer1,3+nvar)-dflux
     unew(ind_buffer2,3+neul)=unew(ind_buffer2,3+neul)-dflux
     unew(ind_buffer2,2+nvar)=unew(ind_buffer2,2+nvar)-dflux
     unew(ind_buffer3,2+neul)=unew(ind_buffer3,2+neul)-dflux
     if(son(ind_buffer1)==0.and.son(ind_buffer2)==0.and.son(ind_buffer3)==0) then
        unew(ind_buffer1,2+nvar)=unew(ind_buffer1,2+nvar)+dflux*0.5
        unew(ind_buffer3,3+neul)=unew(ind_buffer3,3+neul)+dflux*0.5
     endif
  end do

  ! Update coarse By and Bz using fine EMFx on Y=1 and Z=1 grid edge
  ind_father1=1+(i1  )+3*(j1  )+9*(k1+1)
  ind_father2=1+(i1  )+3*(j1+1)+9*(k1+1)
  ind_father3=1+(i1  )+3*(j1+1)+9*(k1  )
  do i=1,ncache
     ind_buffer1=nbors_father_cells(i,ind_father1)
     ind_buffer2=nbors_father_cells(i,ind_father2)
     ind_buffer3=nbors_father_cells(i,ind_father3)
     weight=1.0
     if(son(ind_buffer1)>0.and.son(ind_buffer3)>0) cycle
     if(son(ind_buffer1)>0.or.son(ind_buffer2)>0.or.son(ind_buffer3)>0)weight=0.5
     dflux=(emfx(i,1,3,3)+emfx(i,2,3,3))*0.25*weight
     unew(ind_buffer1,2+nvar)=unew(ind_buffer1,2+nvar)-dflux
     unew(ind_buffer2,2+neul)=unew(ind_buffer2,2+neul)-dflux
     unew(ind_buffer2,3+neul)=unew(ind_buffer2,3+neul)+dflux
     unew(ind_buffer3,3+nvar)=unew(ind_buffer3,3+nvar)+dflux
     if(son(ind_buffer1)==0.and.son(ind_buffer2)==0.and.son(ind_buffer3)==0) then
        unew(ind_buffer3,2+neul)=unew(ind_buffer3,2+neul)+dflux*0.5
        unew(ind_buffer1,3+neul)=unew(ind_buffer1,3+neul)-dflux*0.5
     endif
  end do

  ! Update coarse By and Bz using fine EMFx on Y=1 and Z=0 grid edge
  ind_father1=1+(i1  )+3*(j1+1)+9*(k1  )
  ind_father2=1+(i1  )+3*(j1+1)+9*(k1-1)
  ind_father3=1+(i1  )+3*(j1  )+9*(k1-1)
  do i=1,ncache
     ind_buffer1=nbors_father_cells(i,ind_father1)
     ind_buffer2=nbors_father_cells(i,ind_father2)
     ind_buffer3=nbors_father_cells(i,ind_father3)
     weight=1.0
     if(son(ind_buffer1)>0.and.son(ind_buffer3)>0) cycle
     if(son(ind_buffer1)>0.or.son(ind_buffer2)>0.or.son(ind_buffer3)>0)weight=0.5
     dflux=(emfx(i,1,3,1)+emfx(i,2,3,1))*0.25*weight
     unew(ind_buffer1,3+neul)=unew(ind_buffer1,3+neul)+dflux
     unew(ind_buffer2,3+nvar)=unew(ind_buffer2,3+nvar)+dflux
     unew(ind_buffer2,2+neul)=unew(ind_buffer2,2+neul)+dflux
     unew(ind_buffer3,2+nvar)=unew(ind_buffer3,2+nvar)+dflux
     if(son(ind_buffer1)==0.and.son(ind_buffer2)==0.and.son(ind_buffer3)==0) then
        unew(ind_buffer3,3+nvar)=unew(ind_buffer3,3+nvar)-dflux*0.5
        unew(ind_buffer1,2+neul)=unew(ind_buffer1,2+neul)-dflux*0.5
     endif
  end do

  !--------------------------------------
  ! Deal with 4 EMFy edges
  !--------------------------------------

  ! Update coarse Bx and Bz using fine EMFy on X=0 and Z=0 grid edge
  ind_father1=1+(i1  )+3*(j1  )+9*(k1-1)
  ind_father2=1+(i1-1)+3*(j1  )+9*(k1-1)
  ind_father3=1+(i1-1)+3*(j1  )+9*(k1  )
  do i=1,ncache
     ind_buffer1=nbors_father_cells(i,ind_father1)
     ind_buffer2=nbors_father_cells(i,ind_father2)
     ind_buffer3=nbors_father_cells(i,ind_father3)
     weight=1.0
     if(son(ind_buffer1)>0.and.son(ind_buffer3)>0) cycle
     if(son(ind_buffer1)>0.or.son(ind_buffer2)>0.or.son(ind_buffer3)>0)weight=0.5
     dflux=(emfy(i,1,1,1)+emfy(i,1,2,1))*0.25*weight
     unew(ind_buffer1,1+neul)=unew(ind_buffer1,1+neul)-dflux
     unew(ind_buffer2,1+nvar)=unew(ind_buffer2,1+nvar)-dflux
     unew(ind_buffer2,3+nvar)=unew(ind_buffer2,3+nvar)+dflux
     unew(ind_buffer3,3+neul)=unew(ind_buffer3,3+neul)+dflux
     if(son(ind_buffer1)==0.and.son(ind_buffer2)==0.and.son(ind_buffer3)==0) then
        unew(ind_buffer3,1+nvar)=unew(ind_buffer3,1+nvar)+dflux*0.5
        unew(ind_buffer1,3+nvar)=unew(ind_buffer1,3+nvar)-dflux*0.5
     endif
  end do

  ! Update coarse Bx and Bz using fine EMFy on X=0 and Z=1 grid edge
  ind_father1=1+(i1-1)+3*(j1  )+9*(k1  )
  ind_father2=1+(i1-1)+3*(j1  )+9*(k1+1)
  ind_father3=1+(i1  )+3*(j1  )+9*(k1+1)
  do i=1,ncache
     ind_buffer1=nbors_father_cells(i,ind_father1)
     ind_buffer2=nbors_father_cells(i,ind_father2)
     ind_buffer3=nbors_father_cells(i,ind_father3)
     weight=1.0
     if(son(ind_buffer1)>0.and.son(ind_buffer3)>0) cycle
     if(son(ind_buffer1)>0.or.son(ind_buffer2)>0.or.son(ind_buffer3)>0)weight=0.5
     dflux=(emfy(i,1,1,3)+emfy(i,1,2,3))*0.25*weight
     unew(ind_buffer1,3+nvar)=unew(ind_buffer1,3+nvar)+dflux
     unew(ind_buffer2,3+neul)=unew(ind_buffer2,3+neul)+dflux
     unew(ind_buffer2,1+nvar)=unew(ind_buffer2,1+nvar)+dflux
     unew(ind_buffer3,1+neul)=unew(ind_buffer3,1+neul)+dflux
     if(son(ind_buffer1)==0.and.son(ind_buffer2)==0.and.son(ind_buffer3)==0) then
        unew(ind_buffer3,3+neul)=unew(ind_buffer3,3+neul)-dflux*0.5
        unew(ind_buffer1,1+nvar)=unew(ind_buffer1,1+nvar)-dflux*0.5
     endif
  end do

  ! Update coarse Bx and Bz using fine EMFy on X=1 and Z=1 grid edge
  ind_father1=1+(i1  )+3*(j1  )+9*(k1+1)
  ind_father2=1+(i1+1)+3*(j1  )+9*(k1+1)
  ind_father3=1+(i1+1)+3*(j1  )+9*(k1  )
  do i=1,ncache
     ind_buffer1=nbors_father_cells(i,ind_father1)
     ind_buffer2=nbors_father_cells(i,ind_father2)
     ind_buffer3=nbors_father_cells(i,ind_father3)
     weight=1.0
     if(son(ind_buffer1)>0.and.son(ind_buffer3)>0) cycle
     if(son(ind_buffer1)>0.or.son(ind_buffer2)>0.or.son(ind_buffer3)>0)weight=0.5
     dflux=(emfy(i,3,1,3)+emfy(i,3,2,3))*0.25*weight
     unew(ind_buffer1,1+nvar)=unew(ind_buffer1,1+nvar)+dflux
     unew(ind_buffer2,1+neul)=unew(ind_buffer2,1+neul)+dflux
     unew(ind_buffer2,3+neul)=unew(ind_buffer2,3+neul)-dflux
     unew(ind_buffer3,3+nvar)=unew(ind_buffer3,3+nvar)-dflux
     if(son(ind_buffer1)==0.and.son(ind_buffer2)==0.and.son(ind_buffer3)==0) then
        unew(ind_buffer3,1+neul)=unew(ind_buffer3,1+neul)-dflux*0.5
        unew(ind_buffer1,3+neul)=unew(ind_buffer1,3+neul)+dflux*0.5
     endif
  end do

  ! Update coarse Bx and Bz using fine EMFy on X=1 and Z=0 grid edge
  ind_father1=1+(i1+1)+3*(j1  )+9*(k1  )
  ind_father2=1+(i1+1)+3*(j1  )+9*(k1-1)
  ind_father3=1+(i1  )+3*(j1  )+9*(k1-1)
  do i=1,ncache
     ind_buffer1=nbors_father_cells(i,ind_father1)
     ind_buffer2=nbors_father_cells(i,ind_father2)
     ind_buffer3=nbors_father_cells(i,ind_father3)
     weight=1.0
     if(son(ind_buffer1)>0.and.son(ind_buffer3)>0) cycle
     if(son(ind_buffer1)>0.or.son(ind_buffer2)>0.or.son(ind_buffer3)>0)weight=0.5
     dflux=(emfy(i,3,1,1)+emfy(i,3,2,1))*0.25*weight
     unew(ind_buffer1,3+neul)=unew(ind_buffer1,3+neul)-dflux
     unew(ind_buffer2,3+nvar)=unew(ind_buffer2,3+nvar)-dflux
     unew(ind_buffer2,1+neul)=unew(ind_buffer2,1+neul)-dflux
     unew(ind_buffer3,1+nvar)=unew(ind_buffer3,1+nvar)-dflux
     if(son(ind_buffer1)==0.and.son(ind_buffer2)==0.and.son(ind_buffer3)==0) then
        unew(ind_buffer3,3+nvar)=unew(ind_buffer3,3+nvar)+dflux*0.5
        unew(ind_buffer1,1+neul)=unew(ind_buffer1,1+neul)+dflux*0.5
     endif
  end do
#endif

  end if

end subroutine godfine1
